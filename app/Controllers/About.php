<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class About extends Controller {
	public function fields() {
		return get_fields();
	}
}
