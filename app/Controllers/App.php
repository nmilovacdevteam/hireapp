<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use Walker_nav;

class App extends Controller {
	public static function title(): string {
		$home  = get_option( 'page_for_posts', true );
		$title = '';

		switch ( true ) {
			case is_home() && $home:
				$title = get_the_title( $home );
				break;
			case is_post_type_archive():
				$title = post_type_archive_title( '', false );
				break;
			case is_archive():
				$title = single_term_title( '', false );
				break;
			case is_search():
				$title = sprintf( __( 'Rezultati pretrage za: %s', THEME_TEXT_DOMAIN ), get_search_query() );
				break;
			case is_404():
				$title = __( 'Stranica nije pronađena', THEME_TEXT_DOMAIN );
				break;
			default:
				$title = get_the_title();
		}

		return $title;
	}

	private function navigation( $menu_name, $id, $container_classes = 'navbar-collapse' ) {
		if ( ! has_nav_menu( $menu_name ) ) {
			return false;
		}

		return (object) [
			'nav'     => $logo . '<div class="' . $container_classes . '" id="' . $id . '">' .
			             wp_nav_menu( [
				             'theme_location' => $menu_name,
				             'menu_class'     => 'navbar-nav',
				             'echo'           => false,
				             'walker'         => new Walker_nav()
			             ] ) . '</div>',
			'toggler' => '<button type="button" class="toggler toggler--navbar" data-togle="#' . $id . '" aria-controls="#' . $id . '" aria-expanded="false">
                            <span></span><span></span><span></span></button>'
		];
	}

	public function primaryNavigation() {
		return $this->navigation( 'primary_navigation', 'main-navigation' );
	}

	public function secondaryNavigation() {
		return $this->navigation( 'secondary_navigation', 'secondary-navigation' );
	}

	public function thirdNavigation() {
		return $this->navigation( 'third_navigation', 'third-navigation' );
	}

	public function breadcrumbs() {
		return function_exists( 'yoast_breadcrumb' ) ? yoast_breadcrumb( '<div class="breadcrumbs">', '</div>', false ) : '';
	}

	public function logo(): string {
		$logo = get_field( 'logo', 'option' );
		if ( ! $logo ) {
			return "<a href='" . site_url( '/' ) . "' class='site-logo'>" . get_option( 'blogname' ) . "</a>";
		}

		return "<a href='" . site_url( '/' ) . "' class='site-logo'><img src='' srcset='" . $logo . "' alt=''></a>";
	}

	public function logoDark(): string {
		$logo = get_field( 'logo_dark', 'option' );
		if ( ! $logo ) {
			return "<a href='" . site_url( '/' ) . "' class='site-logo'>" . get_option( 'blogname' ) . "</a>";
		}

		return "<a href='" . site_url( '/' ) . "' class='site-logo logo--dark'><img src='' srcset='" . $logo . "' alt=''></a>";
	}

	public function socials() {
		if ( ! get_field( 'enable_social_sharing', 'option' ) ) {
			return false;
		}

		return (object) [
			( get_field( 'facebook', 'option' ) ?: '' )  => '<svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21" fill="none">
<path d="M21 10.5C21 4.725 16.275 0 10.5 0C4.725 0 0 4.725 0 10.5C0 15.75 3.80625 20.0812 8.79375 20.8687V13.5187H6.16875V10.5H8.79375V8.1375C8.79375 5.5125 10.3688 4.06875 12.7313 4.06875C13.9125 4.06875 15.0938 4.33125 15.0938 4.33125V6.95625H13.7812C12.4688 6.95625 12.075 7.74375 12.075 8.53125V10.5H14.9625L14.4375 13.5187H11.9438V21C17.1938 20.2125 21 15.75 21 10.5Z" fill="white"/>
</svg>',
			( get_field( 'instagram', 'option' ) ?: '' ) => '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
<path d="M10.0022 4.87225C7.16453 4.87225 4.87563 7.16166 4.87563 10C4.87563 12.8383 7.16453 15.1277 10.0022 15.1277C12.8399 15.1277 15.1288 12.8383 15.1288 10C15.1288 7.16166 12.8399 4.87225 10.0022 4.87225ZM10.0022 13.3337C8.16843 13.3337 6.66927 11.8387 6.66927 10C6.66927 8.16133 8.16397 6.6663 10.0022 6.6663C11.8405 6.6663 13.3352 8.16133 13.3352 10C13.3352 11.8387 11.836 13.3337 10.0022 13.3337ZM16.5343 4.6625C16.5343 5.32746 15.9989 5.85853 15.3385 5.85853C14.6737 5.85853 14.1428 5.32299 14.1428 4.6625C14.1428 4.00201 14.6782 3.46647 15.3385 3.46647C15.9989 3.46647 16.5343 4.00201 16.5343 4.6625ZM19.9297 5.87638C19.8539 4.27424 19.488 2.85507 18.3146 1.68582C17.1456 0.516568 15.7267 0.150619 14.1249 0.070289C12.4741 -0.0234297 7.52593 -0.0234297 5.87507 0.070289C4.27775 0.146156 2.8589 0.512105 1.68544 1.68136C0.511991 2.85061 0.150586 4.26978 0.0702733 5.87192C-0.0234244 7.52315 -0.0234244 12.4724 0.0702733 14.1236C0.146124 15.7258 0.511991 17.1449 1.68544 18.3142C2.8589 19.4834 4.27328 19.8494 5.87507 19.9297C7.52593 20.0234 12.4741 20.0234 14.1249 19.9297C15.7267 19.8538 17.1456 19.4879 18.3146 18.3142C19.4835 17.1449 19.8494 15.7258 19.9297 14.1236C20.0234 12.4724 20.0234 7.52761 19.9297 5.87638ZM17.797 15.8953C17.449 16.7701 16.7752 17.4439 15.8963 17.7965C14.58 18.3186 11.4568 18.1981 10.0022 18.1981C8.54769 18.1981 5.41997 18.3142 4.1082 17.7965C3.23369 17.4484 2.55996 16.7745 2.20747 15.8953C1.68544 14.5788 1.80591 11.4549 1.80591 10C1.80591 8.54513 1.68991 5.41671 2.20747 4.10465C2.55549 3.22995 3.22922 2.55606 4.1082 2.2035C5.42443 1.68136 8.54769 1.80185 10.0022 1.80185C11.4568 1.80185 14.5845 1.68582 15.8963 2.2035C16.7708 2.5516 17.4445 3.22548 17.797 4.10465C18.319 5.42118 18.1985 8.54513 18.1985 10C18.1985 11.4549 18.319 14.5833 17.797 15.8953Z" fill="white"/>
</svg>',
			( get_field( 'twitter', 'option' ) ?: '' )   => '<svg xmlns="http://www.w3.org/2000/svg" width="26" height="21" viewBox="0 0 26 21" fill="none">
<path d="M25.4182 2.45105C24.4834 2.86525 23.4793 3.14511 22.4236 3.2716C23.5129 2.61985 24.3278 1.59408 24.7163 0.38567C23.6929 0.993518 22.5729 1.42138 21.405 1.65065C20.6195 0.812028 19.5792 0.256179 18.4455 0.0693972C17.3118 -0.117385 16.1481 0.0753499 15.1352 0.617679C14.1223 1.16001 13.3167 2.02159 12.8436 3.06865C12.3705 4.11572 12.2563 5.28969 12.5188 6.4083C10.4452 6.30418 8.41671 5.76523 6.56491 4.82641C4.71312 3.88759 3.07942 2.56989 1.76985 0.958827C1.32207 1.73125 1.06459 2.6268 1.06459 3.58057C1.06409 4.43918 1.27553 5.28464 1.68015 6.04194C2.08477 6.79923 2.67006 7.44495 3.38409 7.92179C2.55601 7.89544 1.7462 7.67169 1.02206 7.26915V7.33632C1.02197 8.54055 1.43853 9.70773 2.20104 10.6398C2.96355 11.5719 4.02505 12.2114 5.20543 12.45C4.43725 12.6579 3.63186 12.6885 2.85011 12.5395C3.18315 13.5757 3.83186 14.4818 4.70545 15.1309C5.57903 15.7801 6.63375 16.1399 7.72195 16.1598C5.87467 17.6099 3.5933 18.3966 1.24483 18.3931C0.828817 18.3932 0.413161 18.3689 0 18.3203C2.38384 19.8531 5.15879 20.6665 7.99285 20.6633C17.5865 20.6633 22.8311 12.7175 22.8311 5.82618C22.8311 5.60229 22.8255 5.37616 22.8155 5.15228C23.8356 4.41453 24.7162 3.50097 25.4159 2.45441L25.4182 2.45105Z" fill="white"/>
</svg>',
		];
	}

	public static function closeBtn( $targetElement, $classes = '' ): string {
		return '<button type="button" class="btn btn--close ' . $classes . '" data-close="' . $targetElement . '" aria-controls="' . $targetElement . '" aria-expanded="false">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" width="20" height="20">
				<path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 
				189.28 75.93 89.21 c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 
				32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28
		        12.28-32.19 0-44.48L242.72 256z" /></svg></button>';
	}

	public static function toTop( $classes = '' ): string {
		return "<button type='button' class='btn btn--top $classes'>
				<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512' width='20' height='20'>
				<path fill='currentColor' d='M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4
				c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z' /></svg></button>";
	}

	public function newsletter() {
		return (object) [
			'title'   => get_field( 'newsletter_title', 'option' ),
			'content' => get_field( 'newsletter_content', 'option' ),
			'form'    => get_field( 'newsletter_form', 'option' ),
			'image'   => get_field( 'newsletter_image', 'option' )
		];
	}

	public function footerForm() {
		return get_field( 'footer_form', 'option' );
	}

	public function testemonials() {
		return (object) [
			'title'        => get_field( 'testemonial_title', 'option' ),
			'testemonials' => get_field( 'testemonials', 'option' ),
		];
	}

	public function cookie(): string {
		$show_cookie      = get_field( 'show_cookie', 'option' );
		$cookie_title     = get_field( 'cookie_title', 'option' );
		$cookie           = get_field( 'cookie_text', 'option' );
		$btn_text         = get_field( 'btn_text', 'option' ) ?: __( 'Accept', THEME_TEXT_DOMAIN );
		$btn_text_decline = get_field( 'btn_text_decline', 'option' ) ?: __( 'Decline', THEME_TEXT_DOMAIN );
		if ( ! $cookie || ! $show_cookie ) {
			return '';
		}

		return \App\template( 'components.cookie', [
			'show_cookie'      => $show_cookie,
			'cookie_title'     => $cookie_title,
			'cookie_content'   => $cookie,
			'btn_text'         => $btn_text,
			'btn_text_decline' => $btn_text_decline,
		] );
	}
}
