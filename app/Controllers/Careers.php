<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Careers extends Controller {
	public function fields() {
		return get_fields();
	}

	public function openingsLinks()
	{
		$terms = get_terms([
			'taxonomy' => 'opening',
			'hide_empty' => true,
		]);

		return $terms;
	}
}
