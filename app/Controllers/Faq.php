<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Faq extends Controller {
	public function fields() {
		return get_fields();
	}

	public function faqs(): WP_Query {

		$args = [
			'post_type'      => 'faq',
			'posts_per_page' => - 1
		];

		$query = new WP_Query( $args );

		return $query;
	}
}
