<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller {

	public function fields() {
		return get_fields();
	}
}
