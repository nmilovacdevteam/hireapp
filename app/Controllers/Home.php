<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class Home extends Controller {

	public function wordpressQuery(): WP_Query {
		global $wp_query;

		return $wp_query;
	}

	public function getLoadMore(): string {
		$query = $this->wordpressQuery();

		if ( $query->max_num_pages < 2 ):
			return '';
		endif;

		return '<div class="load-more-container"><button type="button" class="btn btn--outline btn--big load-more" data-category="' .
		       $query->get_queried_object()->slug . '">' . __( 'Učitajte još postova', THEME_TEXT_DOMAIN ) . '</button></div>';
	}

	public function title(){
		return get_field('title',get_queried_object());
	}
	public function featured(){
		return get_field('featured_post',get_queried_object());
	}

	public function categoryLinks()
	{
		$terms = get_terms([
			'taxonomy' => 'category',
			'hide_empty' => false,
		]);

		return $terms;
	}
}
