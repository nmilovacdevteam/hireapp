<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use function App\custom_wp_query;

class Single extends Controller {
	public function relatedPosts() {
		$current_post_term = collect( get_the_terms( get_the_ID(), 'category' ) )->map( function ( $term ) {
			return $term->term_id;
		} )->toArray();

		return custom_wp_query( 'post', 4, [ [ 'type' => 'category', 'terms' => $current_post_term, 'field' => 'term_id' ] ] );
	}
}
