<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'about_us' );

$fields->setLocation( 'page_template', '==', 'views/about.blade.php' );

try {
	$fields
		->addTab( 'First Fold', [ 'placement' => 'left' ] )
		->addWysiwyg( 'title', [
			'label' => 'Title'
		] )
		->addImage( 'fold_image', [
			'label' => 'Fold image'
		] )
		->addTab( 'Team', [ 'placement' => 'left' ] )
		->addWysiwyg( 'team_title', [
			'label' => 'Team Title'
		] )
		->addRepeater( 'members', [
			'label' => 'Team Members',
			'layout'=>'block'
		] )
		->addImage( 'member_image', [
			'label' => 'Member image'
		] )
		->addText( 'member_name', [
			'label' => 'Member name'
		] )
		->addText( 'member_role', [
			'label' => 'Member role'
		] );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/about.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
