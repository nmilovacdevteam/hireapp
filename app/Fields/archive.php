<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'archive' );

$fields->setLocation( 'taxonomy', '==', 'category' );

try {
	$fields
		->addWysiwyg('title',[
			'label'=>'Title'
		])
		->addRelationship( 'featured_post', [
			'label'=>'Featured post',
			'max'=>1,
			'return_format'=>'object'
		] );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/frontpage.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
