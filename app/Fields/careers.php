<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'careers' );

$fields->setLocation( 'page_template', '==', 'views/careers.blade.php' );

try {
	$fields
		->addTab( 'First Fold', [ 'placement' => 'left' ] )
		->addWysiwyg( 'title', [
			'label' => 'Title'
		] )
		->addWysiwyg( 'content', [
			'label' => 'Content'
		] )
		->addImage( 'fold_image', [
			'label' => 'Fold image'
		] )
		->addWysiwyg( 'subtext', [
			'label' => 'Subtext'
		] )
		->addTab('Openings',[
			'placement'=>'left'
		])
		->addText('openings_title',[
			'label'=>'Openings Title'
		])
		;
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/about.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
