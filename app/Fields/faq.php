<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'faq' );

$fields->setLocation( 'page_template', '==', 'views/faq.blade.php' );

try {
	$fields
		->addTab( 'First Fold', [ 'placement' => 'left' ] )
		->addWysiwyg( 'title', [
			'label' => 'Title'
		] )
		->addWysiwyg( 'subtext', [
			'label' => 'Subtext'
		] );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/faq.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
