<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'footer_options' );

$fields->setLocation( 'options_page', '==', 'acf-options-footer' );

try {
    $fields
        ->addWysiwyg( 'footer_text', [
            'label'        => __('Footer tekst', THEME_TEXT_DOMAIN),
            'instructions' => '',
            'required'     => 0,
            'tabs'         => 'all',
            'toolbar'      => 'full',
            'media_upload' => 0,
            'delay'        => 1,
        ] );
} catch ( FieldNameCollisionException $e ) {
    printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/footer.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
