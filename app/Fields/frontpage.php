<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'front_page' );

$fields->setLocation( 'post_type', '==', 'page' )
       ->and( 'page_type', '==', 'front_page' );

try {
	$fields
		->addTab( 'First fold', [ 'placement' => 'left' ] )
		->addWysiwyg( 'subtext', [ 'label' => 'Subtext' ] )
		->addWysiwyg( 'title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'content', [ 'label' => 'Content' ] )
		->addImage( 'image', [ 'label' => 'Image' ] )
		->addLink( 'work', [ 'label' => 'Looking for work button' ] )
		->addLink( 'business', [ 'label' => 'Business' ] )
		->addTab( 'Logos', [ 'placement' => 'left' ] )
		->addRepeater( 'logos', [ 'label' => 'Logos' ] )
		->addImage( 'logo', [ 'label' => 'Logo' ] )
		->endRepeater()
		->addTab( 'Stories', [ 'placement' => 'left' ] )
		->addImage( 'stories_image', [ 'label' => 'Image' ] )
		->addWysiwyg( 'stories_title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'stories_content', [ 'label' => 'Content' ] )
		->addLink( 'about_us', [ 'label' => 'About Us' ] )
		->addTab( 'Hire', [ 'placement' => 'left' ] )
		->addImage( 'hire_image', [ 'label' => 'Image' ] )
		->addTab( 'For Business', [ 'placement' => 'left' ] )
		->addText( 'for_business_link', [ 'label' => 'Link' ] )
		->addText( 'for_business_title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'for_business_content', [ 'label' => 'Content' ] )
		->addRepeater( 'for_business', [ 'label' => 'For Business','layout'=>'block' ] )
		->addText( 'service_title', [ 'label' => 'Service title' ] )
		->addWysiwyg( 'service_content', [ 'label' => 'Service content' ] )
		->endRepeater()
		->addTab( 'For Workers', [ 'placement' => 'left' ] )
		->addText( 'for_workers_link', [ 'label' => 'Link' ] )
		->addText( 'for_workers_title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'for_workers_content', [ 'label' => 'Content' ] )
		->addRepeater( 'for_workers', [ 'label' => 'For Workers','layout'=>'block' ] )
		->addText( 'service_title', [ 'label' => 'Service title' ] )
		->addWysiwyg( 'service_content', [ 'label' => 'Service content' ] )
		->endRepeater()
		->addTab( 'FAQ', [ 'placement' => 'left' ] )
		->addWysiwyg( 'faq_title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'faq_content', [ 'label' => 'Content' ] )
		->addImage( 'faq_image', [ 'label' => 'Image' ] )
		->addLink( 'faq_link', [ 'label' => 'Link' ] )

		;
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/frontpage.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
