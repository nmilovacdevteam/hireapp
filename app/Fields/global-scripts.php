<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'global_skripte' );

$fields->setLocation( 'options_page', '==', 'acf-options-dodatne-skripte' );

try {
	$fields
		->addTextarea( 'insert_head', [ 'label' => __( 'Head', THEME_TEXT_DOMAIN ) ] )
		->addTextarea( 'body_scripts', [ 'label' => __( 'Nakon otvorenog body taga', THEME_TEXT_DOMAIN ) ] )
		->addTextarea( 'insert_footer', [ 'label' => __( 'Footer', THEME_TEXT_DOMAIN ) ] );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/global-scripts.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
