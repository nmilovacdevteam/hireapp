<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'global_options' );

$fields->setLocation( 'options_page', '==', 'acf-options-globalna-podesavanja' );

try {
	$fields
		->addTab( 'General', [ 'placement' => 'left' ] )
		->addImage( 'logo', [
			'label'         => __( 'Logo sajta', THEME_TEXT_DOMAIN ),
			'wrapper'       => [
				'width' => '30',
			],
			'return_format' => 'url'
		] )
		->addImage( 'logo_dark', [
			'label'         => __( 'Logo sajta - tamni', THEME_TEXT_DOMAIN ),
			'wrapper'       => [
				'width' => '30',
			],
			'return_format' => 'url'
		] )
		->addImage( 'placeholder', [
			'label'   => __( 'Zamenska slika', THEME_TEXT_DOMAIN ),
			'wrapper' => [
				'width' => '30',
			]
		] )
		->addTab('Newsletter')
		->addImage('newsletter_image',[
			'label'=>'Newsletter Image'
		])
		->addText('newsletter_title',[
			'label'=>'Newsletter title'
		])
		->addText('newsletter_form',[
			'label'=>'Newsletter form'
		])
		->addWysiwyg('newsletter_content',[
			'label'=>'Newsletter content'
		])
		->addText('footer_form',[
			'label'=>'Footer form'
		])
		->addTab('Testemonials',['placement'=>'left'])
		->addWysiwyg('testemonial_title',['label'=>'Title'])
		->addRepeater('testemonials',['label'=>'Testemonials','layout'=>'block'])
		->addText('testemonial_title',['label'=>'Title'])
		->addWysiwyg('testemonial_content',['label'=>'Content'])
		->addText('testemonial_name',['label'=>'Name'])
		->addText('testemonial_role',['label'=>'Role'])
		->endRepeater()
		->addFields( get_field_partial( 'partials.cookie' ) )
		->addFields( get_field_partial( 'partials.socials' ) );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/global.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
