<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$builder = new FieldsBuilder( 'builder' );

try {
    $builder
        ->addTab( 'builder', [ 'placement' => 'left' ] )
        ->addFlexibleContent( 'components', [ 'button_label' => __('Dodaj komponentu'), THEME_TEXT_DOMAIN ] )
        ->addLayout( get_field_partial( 'components.button' ) );
} catch ( FieldNameCollisionException $e ) {
    printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/partials/builder.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $builder;
