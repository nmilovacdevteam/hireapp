<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'cookie' );

try {
	$fields
		->addTab( 'kola&ccaron;i&cacute;i', [ 'placement' => 'left' ] )
		->addTrueFalse( 'show_cookie', [
			'instructions'  => __( 'Prikaži/Sakrij kolačiće.', THEME_TEXT_DOMAIN ),
			'ui'            => 1,
			'default_value' => 0,
			'ui_on_text'    => __( 'Prikaži', THEME_TEXT_DOMAIN ),
			'ui_off_text'   => __( 'Sakrij', THEME_TEXT_DOMAIN ),
		] )
		->addText( 'cookie_title', [
			'label' => __( 'Naslov za kolačiće', THEME_TEXT_DOMAIN ),
		] )
		->addWysiwyg( 'cookie_text', [
			'label'        => __( 'Tekst za kolačiće', THEME_TEXT_DOMAIN ),
			'delay'        => 1,
			'media_upload' => 0,
		] )
		->addText( 'btn_text', [ 'label' => __( 'Tekst za prihvatanje', THEME_TEXT_DOMAIN ) ] )
		->addText( 'btn_text_decline', [ 'label' => __( 'Tekst za odbijanje', THEME_TEXT_DOMAIN ) ] );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/partials/cookie.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
