<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'socials' );

try {
	$fields
		->addTab( 'socials', [ 'placement' => 'left' ] )
		->addTrueFalse( 'enable_social_sharing', [
			'instructions'  => __( 'Prikaži linkove do društvenih mreža.', THEME_TEXT_DOMAIN ),
			'ui'            => 1,
			'default_value' => 0,
			'ui_on_text'    => __( 'Prikaži', THEME_TEXT_DOMAIN ),
			'ui_off_text'   => __( 'Sakrij', THEME_TEXT_DOMAIN ),
		] )
		->addUrl( 'facebook', [
			'label'             => __( 'Facebook URL', THEME_TEXT_DOMAIN ),
			'conditional_logic' => [
				[
					'field'    => 'enable_social_sharing',
					'operator' => '==',
					'value'    => '1'
				]
			],
			'placeholder'       => __( 'Unesite Facebook adresu', THEME_TEXT_DOMAIN ),
		] )
		->addUrl( 'instagram', [
			'label'             => __( 'Instagram URL', THEME_TEXT_DOMAIN ),
			'conditional_logic' => [
				[
					'field'    => 'enable_social_sharing',
					'operator' => '==',
					'value'    => '1'
				]
			],
			'placeholder'       => __( 'Unesite Instagram adresu', THEME_TEXT_DOMAIN ),
		] )
		->addUrl( 'twitter', [
			'label'             => __( 'Twitter URL', THEME_TEXT_DOMAIN ),
			'conditional_logic' => [
				[
					'field'    => 'enable_social_sharing',
					'operator' => '==',
					'value'    => '1'
				]
			],
			'placeholder'       => __( 'Unesite YouTube adresu', THEME_TEXT_DOMAIN ),
		] );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/partials/socials.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
