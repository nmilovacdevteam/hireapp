<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'careers_single' );

$fields->setLocation( 'post_type', '==', 'careers' );

try {
	$fields
		->addTab( 'Info', [ 'placement' => 'left' ] )
		->addText('location',[
			'label'=>'Location'
		])
		;
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/single-careers.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
