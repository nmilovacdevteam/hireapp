<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'workers' );

$fields->setLocation( 'page_template', '==', 'views/workers.blade.php' );

try {
	$fields
		->addTab('Colors',['placement'=>'left'])
		->addTrueFalse('picker', [
			'label' => 'Workers / Business',
			'default_value' => 0,
			'ui' => 0,
			'ui_on_text' => 'Business',
			'ui_off_text' => 'Workers',
		])

		->addTab( 'First fold', [ 'placement' => 'left' ] )
		->addWysiwyg( 'subtext', [ 'label' => 'Subtext' ] )
		->addWysiwyg( 'title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'content', [ 'label' => 'Content' ] )
		->addImage( 'image', [ 'label' => 'Image' ] )
		->addLink( 'link', [ 'label' => 'Link' ] )


		->addTab( 'Services', [ 'placement' => 'left' ] )
		->addRepeater( 'services', [ 'label' => 'List' ] )
		->addText( 'services_title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'services_content', [ 'label' => 'Content' ] )
		->addImage( 'services_image', [ 'label' => 'Image' ] )
		->endRepeater()


		->addTab( 'Hospitality', [ 'placement' => 'left' ] )
		->addWysiwyg( 'hospitality_title', [ 'label' => 'Title' ] )
		->addImage( 'hospitality_image', [ 'label' => 'Image' ] )
		->addRepeater( 'list', [ 'label' => 'List' ] )
		->addText( 'list_title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'list_content', [ 'label' => 'Content' ] )
		->endRepeater()
		->addLink('hospitality_link',['label'=>'Link'])


		->addTab( 'Download', [ 'placement' => 'left' ] )
		->addWysiwyg( 'download_content', [ 'label' => 'Download Content' ] )
		->addUrl( 'appstore', [ 'label' => 'Appstore' ] )
		->addUrl( 'playstore', [ 'label' => 'Appstore' ] )

		->addTab( 'Logos', [ 'placement' => 'left' ] )
		->addText( 'logos_title', [ 'label' => 'Title' ] )
		->addRepeater( 'logos', [ 'label' => 'Logos' ] )
		->addImage( 'logo', [ 'label' => 'Logo' ] )
		->endRepeater()

		->addTab( 'FAQ', [ 'placement' => 'left' ] )
		->addWysiwyg( 'faq_title', [ 'label' => 'Title' ] )
		->addWysiwyg( 'faq_content', [ 'label' => 'Content' ] )
		->addImage( 'faq_image', [ 'label' => 'Image' ] )
		->addLink( 'faq_link', [ 'label' => 'Link' ] )

		->addTab('How it works',['placement'=>'left'])
		->addText('how_it_works_title',['label'=>'Title'])
		->addRepeater('how_it_works_content',['label'=>'Testemonials','layout'=>'block'])
		->addText('how_it_works_content_title',['label'=>'Title'])
		->addText('how_it_works_content_content',['label'=>'Content'])
		->endRepeater()
		->addImage('how_it_works_image',['label'=>'Image'])

		;
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/about.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;
