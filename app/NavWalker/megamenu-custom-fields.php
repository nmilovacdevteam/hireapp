<?php

// Create fields
// Show columns
// Save/Update fields
// Update the Walker nav

function fields_list() {
	//note that menu-item- gets prepended to field names
	//i.e.: field-01 becomes menu-item-field-01
	//i.e.: icon-url becomes menu-item-icon-url
	return [
		'mm-megamenu'       => __( 'Aktiviraj mega menu', THEME_TEXT_DOMAIN ),
		'mm-acf-field'      => __( 'Aktiviraj ACF', THEME_TEXT_DOMAIN ),
		'mm-column-divider' => __( 'Završetak kolone', THEME_TEXT_DOMAIN ),
		'mm-row-divider'    => __( 'Razdvajač redova', THEME_TEXT_DOMAIN ),
	];
}

// Setup fields
// Kada se pozove add_filter( 'wp_edit_nav_menu_walker', function ( $walker ){}), tada duplira polja
add_action( 'wp_nav_menu_item_custom_fields', function ( $id, $item, $depth, $args ) {

	$fields    = fields_list();

	foreach ( $fields as $_key => $label ) :
		$key = sprintf( 'menu-item-%s', $_key );
		$id    = sprintf( 'edit-%s-%s', $key, $item->ID );
		$name  = sprintf( '%s[%s]', $key, $item->ID );
		$value = get_post_meta( $item->ID, $key, true );
		$class = sprintf( 'field-%s', $_key );
		?>
		<p class="description description-wide <?php echo esc_attr( $class ) ?>">
			<label for="<?php echo esc_attr( $id ); ?>">
				<input type="checkbox" id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>" value="1"
					<?php echo ( $value == 1 ) ? 'checked="checked"' : ''; ?> /><?php echo esc_attr( $label ); ?>
			</label>
		</p>
	<?php
	endforeach;

}, 10, 4 );

// Create Columns
add_filter( 'manage_nav-menus_columns', function ( $columns ) {

	$fields = fields_list();

	$columns = array_merge( $columns, $fields );

	return $columns;
}, 99 );

// Save fields
add_action( 'wp_update_nav_menu_item', function ( $menu_id, $menu_item_db_id, $menu_item_args ) {
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}

	check_admin_referer( 'update-nav_menu', 'update-nav-menu-nonce' );

	$fields = fields_list();

	foreach ( $fields as $_key => $label ) {
		$key = sprintf( 'menu-item-%s', $_key );

		// Do sanitization if needed
		if ( !empty( $_POST[ $key ][ $menu_item_db_id ] ) ) {
			$value = $_POST[ $key ][ $menu_item_db_id ];
		} else {
			$value = null;
		}

		// Update.
		if ( !is_null( $value ) ) {
			update_post_meta( $menu_item_db_id, $key, $value );
//            echo "key:$key<br />";
		} else {
			delete_post_meta( $menu_item_db_id, $key );
		}
	}
}, 10, 3 );

//add_filter( 'wp_edit_nav_menu_walker', function ( $walker ) {
//    $walker = 'MegaMenu_Walker_Edit';
//    if ( ! class_exists( $walker ) ) {
//        require_once dirname( __FILE__ ) . '/walker-nav-menu-edit.php';
//    }
//
//    return $walker;
//}, 99 );
