<?php

namespace App;

/**
 * Theme customizer
 */
add_action( 'customize_register', function ( \WP_Customize_Manager $wp_customize ) {
	// Add postMessage support
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->selective_refresh->add_partial( 'blogname', [
		'selector'        => '.brand',
		'render_callback' => function () {
			bloginfo( 'name' );
		}
	] );
} );

/**
 * Customizer JS
 */
add_action( 'customize_preview_init', function () {
	wp_enqueue_script( 'customizer.js', asset_path( 'scripts/customizer.js' ), [ 'customize-preview' ], null, true );
} );

add_action( 'admin_head', function () {
	?>
	<style type="text/css">
		#adminmenu .toplevel_page_acf-options-globalna-podesavanja .dashicons-before img {
			padding: 5px 0 !important;
			width:   19px;
		}

		#edittag {
			max-width: 75%;
		}
	</style>
	<?php
} );


/**
 * Setup WordPress after theme activation
 */
add_action( 'after_switch_theme', function () {
	// Get options
	$is_theme_already_activated = (int) get_option( 'first_time_activation' );

	// If theme is not activated first time, don't overwrite settings and pages
	if ( $is_theme_already_activated ) {
		return;
	}

	// Create pages for page templates in theme
	$pages_to_create = [
		[
			'post_type'    => 'page',
			'post_author'  => get_current_user_id(),
			'post_title'   => __( 'Početna', THEME_TEXT_DOMAIN ),
			'post_content' => __( 'Statički tekst ...', THEME_TEXT_DOMAIN ),
			'post_status'  => 'publish'
		],
		[
			'post_type'     => 'page',
			'post_author'   => get_current_user_id(),
			'post_title'    => __( 'Kontakt', THEME_TEXT_DOMAIN ),
			'post_content'  => __( 'Statički tekst ...', THEME_TEXT_DOMAIN ),
			'post_status'   => 'publish',
			'page_template' => 'kontakt',
		],
		[
			'post_type'     => 'page',
			'post_author'   => get_current_user_id(),
			'post_title'    => __( 'Specifikacija', THEME_TEXT_DOMAIN ),
			'post_content'  => __( 'Statički tekst ...', THEME_TEXT_DOMAIN ),
			'post_status'   => 'publish',
			'page_template' => 'specifikacija',
		],
		[
			'post_type'    => 'page',
			'post_author'  => get_current_user_id(),
			'post_title'   => __( 'Blog', THEME_TEXT_DOMAIN ),
			'post_content' => '',
			'post_status'  => 'publish'
		]
	];
	foreach ( $pages_to_create as $page ) {
		$page = wp_insert_post( $page );
		if ( is_wp_error( $page ) ) {
			printf( __( 'Greška tokom pravljenja stranice: %s', THEME_TEXT_DOMAIN ), is_wp_error( $page->get_error_message() ) );
		}
	}

	$front_page = get_page_by_title( 'Početna', 'OBJECT', 'page' );
	$blog_page  = get_page_by_title( 'Blog', 'OBJECT', 'page' );

	$fresh_site_options = [
		'page_for_posts'         => $blog_page->ID,
		'page_on_front'          => $front_page->ID,
		'show_on_front'          => 'page',
		'blogdescription'        => '',
		'default_comment_status' => 'closed',
		'default_ping_status'    => 'closed',
		'date_format'            => __( 'd/m/Y', THEME_TEXT_DOMAIN ),
		'time_format'            => __( 'H:i', THEME_TEXT_DOMAIN ),
		//        'blog_public'            => '0',
		'permalink_structure'    => '/%category%/%postname%/',
	];

	foreach ( $fresh_site_options as $option => $option_value ) {
		update_option( $option, $option_value );
	}

	// Remove WordPress Hello World post, and Sample page
	wp_delete_post( '1', true );
	wp_delete_post( '2', true );

	// Update WordPress permalinks after updating options
	flush_rewrite_rules();

	add_action( 'admin_notices', function () {
		$front             = get_page_by_title( 'Početna', 'OBJECT', 'page' );
		$front_page_link   = sprintf( '<a href="%s">%s</a>', get_edit_post_link( $front->ID ), $front->post_name );
		$blog              = get_page_by_title( 'Blog', 'OBJECT', 'page' );
		$blog_page_link    = sprintf( '<a href="%s">%s</a>', get_edit_post_link( $blog->ID ), $blog->post_name );
		$kontakt           = get_page_by_title( 'Kontakt', 'OBJECT', 'page' );
		$kontakt_page_link = sprintf( '<a href="%s">%s</a>', get_edit_post_link( $kontakt->ID ), $kontakt->post_name );

		?>
		<div class="notice notice-info">
			<p><?php echo sprintf( __( 'Tema %s je izmenila:', THEME_TEXT_DOMAIN ), get_option( 'current_theme' ) ); ?></p>
			<ul>
				<li><?php echo sprintf( __( 'Osnovna <a href="%s/options-general.php">podešavanja</a> WordPress-a kao što su:', THEME_TEXT_DOMAIN ), admin_url() ); ?>
					<ul>
						<li><?php _e( 'Uklanja osnovni podnaslov sajta ,,Još jedno WordPress mesto,,', THEME_TEXT_DOMAIN ); ?></li>
						<li><?php _e( 'Menja jezik na srpski', THEME_TEXT_DOMAIN ); ?></li>
						<li><?php _e( 'Postavlja datum u formatu dan/mesec/godina', THEME_TEXT_DOMAIN ); ?></li>
						<li><?php _e( 'Postavlja vreme u formatu 20:00', THEME_TEXT_DOMAIN ); ?></li>
						<li><?php _e( 'Postavlja statičke stranice za početnu i stranicu bloga', THEME_TEXT_DOMAIN ); ?></li>
						<!--                        <li>--><?php //_e( 'Uključuje deindeksiranje od strane pretraživača', THEME_TEXT_DOMAIN ); ?><!--</li>-->
						<li><?php _e( 'Ažurira permalinkove u format /%category%/%postname%/ i osvežava ih', THEME_TEXT_DOMAIN ); ?></li>
					</ul>
				</li>
				<li><?php _e( 'Obrisala je Hello World članak i stranicu Sample Page.', THEME_TEXT_DOMAIN ); ?></li>
				<li><?php echo sprintf( __( 'Napravila je osnovne stranice kao što su %s, %s, %s', THEME_TEXT_DOMAIN ), $front_page_link, $blog_page_link, $kontakt_page_link ); ?></li>
				<li><?php echo sprintf( __( 'Napravila je navigaciju sa napravljenim stranicama.', THEME_TEXT_DOMAIN ) ); ?></li>
			</ul>
		</div>
		<?php
	} );

	$menuname      = __( 'Glavna navigacija', THEME_TEXT_DOMAIN );
	$menu_location = 'primary_navigation';
	$pages         = [
		get_page_by_title( 'Početna', 'OBJECT', 'page' ),
		get_page_by_title( 'Blog', 'OBJECT', 'page' ),
		get_page_by_title( 'Kontakt', 'OBJECT', 'page' ),
		get_page_by_title( 'Specifikacija', 'OBJECT', 'page' )
	];

	// Check of nav already exists
	if ( !wp_get_nav_menu_object( $menuname ) ) {
		$menu_id = wp_create_nav_menu( $menuname );

		// Set up default BuddyPress links and add them to the menu.
		foreach ( $pages as $page ) {
			wp_update_nav_menu_item( $menu_id, 0, [
				'menu-item-title'   => $page->post_title,
				'menu-item-classes' => $page->post_name,
				'menu-item-url'     => get_page_link( $page ),
				'menu-item-status'  => 'publish'
			] );
		}

		// Grab the theme locations and assign our newly-created menu
		// to the BuddyPress menu location.
		if ( !has_nav_menu( $menu_location ) ) {
			$locations                   = get_theme_mod( 'nav_menu_locations' );
			$locations[ $menu_location ] = $menu_id;
			set_theme_mod( 'nav_menu_locations', $locations );
		}
	}

// Add custom option to track if theme is first time activated so it can add custom pages and settings to WordPress
	add_option( 'first_time_activation', 1, '', false );
} );

/**
 * Defining ACF Options Page
 */
add_action( 'acf/init', function () {
	if ( function_exists( 'acf_add_options_page' ) ) {
		acf_add_options_page( [
			'page_title' => __( 'Theme', THEME_TEXT_DOMAIN ),
			'menu_title' => __( 'Theme', THEME_TEXT_DOMAIN ),
			'menu_slug'  => 'b22-theme',
			'capability' => 'edit_posts',
			'redirect'   => true,
			'autoload'   => true,
		] );

		acf_add_options_sub_page( [
			'page_title'  => __( 'Globalna Podešavanja', THEME_TEXT_DOMAIN ),
			'menu_title'  => __( 'Globalna Podešavanja', THEME_TEXT_DOMAIN ),
			'parent_slug' => 'b22-theme',
		] );

		acf_add_options_sub_page( [
			'page_title'  => __( 'Dodatne skripte', THEME_TEXT_DOMAIN ),
			'menu_title'  => __( 'Dodatne skripte', THEME_TEXT_DOMAIN ),
			'parent_slug' => 'b22-theme',
		] );

		acf_add_options_sub_page( [
			'page_title'  => __( 'Podešavanje Header-a', THEME_TEXT_DOMAIN ),
			'menu_title'  => __( 'Header', THEME_TEXT_DOMAIN ),
			'parent_slug' => 'b22-theme',
		] );

		acf_add_options_sub_page( [
			'page_title'  => __( 'Podešavanje Footer-a', THEME_TEXT_DOMAIN ),
			'menu_title'  => __( 'Footer', THEME_TEXT_DOMAIN ),
			'parent_slug' => 'b22-theme',
		] );
	}
} );
