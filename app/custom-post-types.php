<?php

add_action( 'init', function () {
	// Post Types
	$post_types = [
		[
			'type'       => 'faq',
			'singular'   => __( 'FAQ', THEME_TEXT_DOMAIN ),
			'plural'     => __( 'FAQS', THEME_TEXT_DOMAIN ),
			'taxonomies' => [ 'type' ],
			'supports'   => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
			'rewrite'    => [ 'slug' => 'type', 'with_front' => false ],
		],
		[
			'type'       => 'careers',
			'singular'   => __( 'Career', THEME_TEXT_DOMAIN ),
			'plural'     => __( 'Careers', THEME_TEXT_DOMAIN ),
			'taxonomies' => [ 'opening' ],
			'supports'   => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
			'rewrite'    => [ 'slug' => 'type', 'with_front' => false ],
		],
	];
	foreach ( $post_types as $post_type ) {
		register_post_type( $post_type['type'], [
			'label'               => $post_type['plural'],
			'labels'              => [
				'name'                  => $post_type['plural'],
				'singular_name'         => $post_type['singular'],
				'menu_name'             => $post_type['plural'],
				'name_admin_bar'        => $post_type['plural'],
				'archives'              => sprintf( '%s Archives', $post_type['singular'] ),
				'attributes'            => sprintf( '%s Attributes', $post_type['singular'] ),
				'parent_item_colon'     => __( 'Parent Item:', THEME_TEXT_DOMAIN ),
				'all_items'             => sprintf( 'All %s', $post_type['plural'] ),
				'add_new_item'          => sprintf( 'Add New %s', $post_type['singular'] ),
				'add_new'               => __( 'Add New', THEME_TEXT_DOMAIN ),
				'new_item'              => sprintf( 'New %s', $post_type['singular'] ),
				'edit_item'             => sprintf( 'Edit %s', $post_type['singular'] ),
				'update_item'           => sprintf( 'Update %s', $post_type['singular'] ),
				'view_item'             => sprintf( 'View %s', $post_type['singular'] ),
				'view_items'            => sprintf( 'View %s', $post_type['plural'] ),
				'search_items'          => sprintf( 'Search %s', $post_type['plural'] ),
				'not_found'             => __( 'Not found', THEME_TEXT_DOMAIN ),
				'not_found_in_trash'    => __( 'Not found in Trash', THEME_TEXT_DOMAIN ),
				'featured_image'        => __( 'Featured Image', THEME_TEXT_DOMAIN ),
				'set_featured_image'    => __( 'Set featured image', THEME_TEXT_DOMAIN ),
				'remove_featured_image' => __( 'Remove featured image', THEME_TEXT_DOMAIN ),
				'use_featured_image'    => __( 'Use as featured image', THEME_TEXT_DOMAIN ),
				'insert_into_item'      => __( 'Insert into item', THEME_TEXT_DOMAIN ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', THEME_TEXT_DOMAIN ),
				'items_list'            => __( 'Items list', THEME_TEXT_DOMAIN ),
				'items_list_navigation' => __( 'Items list navigation', THEME_TEXT_DOMAIN ),
				'filter_items_list'     => __( 'Filter items list', THEME_TEXT_DOMAIN ),
			],
			'supports'            => $post_type['supports'] ?? [],
			'taxonomies'          => $post_type['taxonomies'] ?? [],
			'rewrite'             => $post_type['rewrite'] ?? true,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 20,
			'menu_icon'           => $post_type['menu_icon'] ?? [],
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		] );
	}

	// Taxonomies
	$taxonomies = [
		[
			'type'     => 'type',
			'singular' => __( 'Type', THEME_TEXT_DOMAIN ),
			'plural'   => __( 'Types', THEME_TEXT_DOMAIN ),
			'cpt'      => [ 'faq' ],
			'rewrite'  => [
				'slug'       => 'type',
				'with_front' => true
			],
			],[
			'type'     => 'opening',
			'singular' => __( 'Opening', THEME_TEXT_DOMAIN ),
			'plural'   => __( 'Openings', THEME_TEXT_DOMAIN ),
			'cpt'      => [ 'careers' ],
			'rewrite'  => [
				'slug'       => 'opening',
				'with_front' => true
			]
		]
	];
	foreach ( $taxonomies as $taxonomy ) {
		register_taxonomy( $taxonomy['type'], $taxonomy['cpt'] ?? [], [
			'labels'            => [
				'name'                       => $taxonomy['plural'],
				'singular_name'              => $taxonomy['singular'],
				'menu_name'                  => $taxonomy['plural'],
				'all_items'                  => sprintf( 'All %s', $taxonomy['plural'] ),
				'parent_item'                => __( 'Parent Item', THEME_TEXT_DOMAIN ),
				'parent_item_colon'          => __( 'Parent Item:', THEME_TEXT_DOMAIN ),
				'new_item_name'              => sprintf( 'New %s Name', $taxonomy['singular'] ),
				'add_new_item'               => sprintf( 'Add New %s', $taxonomy['singular'] ),
				'edit_item'                  => sprintf( 'Edit %s', $taxonomy['singular'] ),
				'update_item'                => sprintf( 'Update %s', $taxonomy['singular'] ),
				'view_item'                  => sprintf( 'View %s', $taxonomy['singular'] ),
				'separate_items_with_commas' => __( 'Separate items with commas', THEME_TEXT_DOMAIN ),
				'add_or_remove_items'        => sprintf( 'Add or remove %s', $taxonomy['plural'] ),
				'choose_from_most_used'      => __( 'Choose from the most used', THEME_TEXT_DOMAIN ),
				'popular_items'              => __( 'Popular Items', THEME_TEXT_DOMAIN ),
				'search_items'               => __( 'Search Items', THEME_TEXT_DOMAIN ),
				'not_found'                  => __( 'Not Found', THEME_TEXT_DOMAIN ),
				'no_terms'                   => __( 'No items', THEME_TEXT_DOMAIN ),
				'items_list'                 => __( 'Items list', THEME_TEXT_DOMAIN ),
				'items_list_navigation'      => __( 'Items list navigation', THEME_TEXT_DOMAIN ),
			],
			'rewrite'           => $taxonomy['rewrite'] ?? false,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
		] );
	}

	flush_rewrite_rules();
} );
