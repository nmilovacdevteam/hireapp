<?php

namespace App;

use Walker_nav;

/**
 * Add <body> classes
 */
add_filter( 'body_class', function ( array $classes ) {
	/** Add page slug if it doesn't exist */
	if ( is_single() || is_page() && !is_front_page() ) {
		if ( !in_array( basename( get_permalink() ), $classes ) ) {
			$classes[] = basename( get_permalink() );
		}
	}

	/** Add class if sidebar is active */
	if ( display_sidebar() ) {
		$classes[] = 'sidebar-primary';
	}

	/** Clean up class names for custom templates */
	$classes = array_map( function ( $class ) {
		return preg_replace( [ '/-blade(-php)?$/', '/^page-template-views/' ], '', $class );
	}, $classes );

	return array_filter( $classes );
} );

/**
 * Template Hierarchy should search for .blade.php files
 */
collect( [
	'index',
	'404',
	'archive',
	'author',
	'category',
	'tag',
	'taxonomy',
	'date',
	'home',
	'frontpage',
	'page',
	'paged',
	'search',
	'single',
	'singular',
	'attachment',
	'embed'
] )->map( function ( $type ) {
	add_filter( "{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates' );
} );

/**
 * Render page using Blade
 */
add_filter( 'template_include', function ( $template ) {
	collect( [ 'get_header', 'wp_head' ] )->each( function ( $tag ) {
		ob_start();
		do_action( $tag );
		$output = ob_get_clean();
		remove_all_actions( $tag );
		add_action( $tag, function () use ( $output ) {
			echo $output;
		} );
	} );
	$data = collect( get_body_class() )->reduce( function ( $data, $class ) use ( $template ) {
		return apply_filters( "sage/template/{$class}/data", $data, $template );
	}, [] );
	if ( $template ) {
		echo template( $template, $data );

		return get_stylesheet_directory() . '/index.php';
	}

	return $template;
}, PHP_INT_MAX );

/**
 * Render comments.blade.php
 */
add_filter( 'comments_template', function ( $comments_template ) {
	$comments_template = str_replace(
		[ get_stylesheet_directory(), get_template_directory() ],
		'',
		$comments_template
	);

	$data = collect( get_body_class() )->reduce( function ( $data, $class ) use ( $comments_template ) {
		return apply_filters( "sage/template/{$class}/data", $data, $comments_template );
	}, [] );

	$theme_template = locate_template( [ "views/{$comments_template}", $comments_template ] );

	if ( $theme_template ) {
		echo template( $theme_template, $data );

		return get_stylesheet_directory() . '/index.php';
	}

	return $comments_template;
}, 100 );

// Woocommerce filters
/*
 *  Remove woocommerce default styles
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
// To change add to cart text on single product page
//add_filter( 'woocommerce_product_single_add_to_cart_text', function () {
//	return __( 'Dodajte u korpu', THEME_TEXT_DOMAIN );
//} );

// To change add to cart text on shop/archive page
//add_filter( 'woocommerce_product_add_to_cart_text', function () {
//	return __( 'Dodaj u korpu', THEME_TEXT_DOMAIN );
//} );

// Uklanjamo skroz default ordering i pravimo sopstveni
//add_filter( 'woocommerce_catalog_orderby', function ( $ordering ) {
//	return [
//		'menu_order title' => $ordering[ 'menu_order' ],
//		'title-asc'        => __( 'Naziv proizvoda A-Z', THEME_TEXT_DOMAIN ),
//		'title-desc'       => __( 'Naziv proizvoda Z-A', THEME_TEXT_DOMAIN ),
//		'price-asc'        => $ordering[ 'price' ],
//		'price-desc'       => $ordering[ 'price-desc' ]
//	];
//} );
// Change woocommerce widget container element recently viewed products
//add_filter( 'woocommerce_before_widget_product_list', function () {
//	return '<div class="product_list_widget">';
//} );
//
//add_filter( 'woocommerce_after_widget_product_list', function () {
//	return '</div>';
//} );
// Change number of related products
//add_filter( 'woocommerce_output_related_products_args', function ( $args ) {
//	$args[ 'posts_per_page' ] = 3;
//
//	return $args;
//}, 20 );
//add_filter( 'loop_shop_per_page', function ( $cols ) {
//	$cols = 12;
//	return $cols;
//}, 20 );
//add_filter( 'woocommerce_checkout_fields', function ( $fields ) {
//	unset( $fields[ 'order' ][ 'order_comments' ] );
//	unset( $fields[ 'billing' ][ 'billing_state' ] );
//
//	return $fields;
//} );
//add_filter( 'woocommerce_currency_symbol', function ( $currency_symbol, $currency ) {
//	return $currency === 'RSD' ? 'RSD' : $currency_symbol;
//}, 10, 2 );
//add_filter( 'woocommerce_default_address_fields', function ( $address_fields ) {
//	$address_fields[ 'first_name' ][ 'placeholder' ] = __( 'Ime *', THEME_TEXT_DOMAIN );
//	$address_fields[ 'last_name' ][ 'placeholder' ]  = __( 'Prezime *', THEME_TEXT_DOMAIN );
//	$address_fields[ 'address_1' ][ 'placeholder' ]  = __( 'Ulica i kućni broj *', THEME_TEXT_DOMAIN );
//	$address_fields[ 'address_2' ][ 'placeholder' ]  = __( 'Sprat/Stan *', THEME_TEXT_DOMAIN );
//	$address_fields[ 'address_2' ][ 'required' ]     = true;
//	$address_fields[ 'city' ][ 'placeholder' ]       = __( 'Grad *', THEME_TEXT_DOMAIN );
//	$address_fields[ 'postcode' ][ 'placeholder' ]   = __( 'Poštanski broj *', THEME_TEXT_DOMAIN );
//	$address_fields[ 'state' ][ 'placeholder' ]      = __( 'Region', THEME_TEXT_DOMAIN );
//	$address_fields[ 'country' ][ 'postcode' ]       = __( 'Država', THEME_TEXT_DOMAIN );
//	return $address_fields;
//}, 20, 1 );
add_filter( 'woocommerce_form_field_args', function ( $input ) {
	array_push( $input[ 'class' ], 'form-group' );

//	$input[ 'placeholder' ]   = $input[ 'label' ];
//	$input[ 'placeholder' ] = '';

	return $input;
} );
/**
 * Edit woocommerce product tabs
 */
//add_filter( 'woocommerce_product_tabs', function ( $tabs ) {
//
//	unset( $tabs[ 'description' ] );               // Remove the description tab
//	unset( $tabs[ 'reviews' ] );                   // Remove the reviews tab
//	unset( $tabs[ 'additional_information' ] );    // Remove the additional information tab
//
//	return $tabs;
//}, 98 );


// Yoast Seo breadcrumb output
//add_filter( 'wpseo_breadcrumb_output', function ( $output ) {
//	return preg_replace( '|</?span>|', '', $output );
//} );
//add_filter( 'wpseo_breadcrumb_output_wrapper', function ( $t ) {
//	return 'p';
//} );
add_filter( 'wpseo_breadcrumb_separator', function ( $separator ) {
	return "<span class='separator'>$separator</span>";
} );

// Wordpress filters
// Stop cf7 from adding paragraphs and breaks
add_filter( 'wpcf7_autop_or_not', '__return_false' );
add_filter( 'use_block_editor_for_post', '__return_false' );
// Remove auto update check for plugins and themes
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );
//add_filter( 'excerpt_length', function ( $length ) {
//	return 30;
//}, 999 );
add_filter( 'excerpt_more', function ( $more ) {
	return '...';
}, 999 );
add_filter( 'acf_the_content', function ( $content ) {
	return str_replace( 'src="', 'src="" data-srcset="', $content );
} );
// Change wordpress images quality
//add_filter( 'jpeg_quality', function () {
//	return 100;
//} );
// Override Walker for widget navigation
add_filter( 'wp_nav_menu_args', function ( $args ) {
	return array_merge( $args, [
		'walker' => new Walker_nav(),
	] );
} );
// Unset/Disable unwanted image sizes
add_filter( 'intermediate_image_sizes_advanced', function ( $sizes ) {
	unset( $sizes[ 'medium' ] );
	unset( $sizes[ 'medium_large' ] );
	unset( $sizes[ 'large' ] );
	return $sizes;
} );
//add_filter( 'image_size_names_choose', function ( $sizes ) {
//	/**
//	 * @param $sizes
//	 *
//	 * @return array
//	 */
//	return array_merge( $sizes, [
//		'image-name' => __( 'Image name', THEME_TEXT_DOMAIN ),
//	] );
//} );
// Adding defer attribute to scripts if user is not logged in
if ( !is_user_logged_in() ) {
	add_filter( 'script_loader_tag', function ( $tag, $handle ) {
		/**
		 * @param $tag
		 * @param $handle
		 *
		 * @return string|string[]
		 */
		$deffered_script = collect( [
			'main.js'
		] )->filter( function ( $script ) use ( $handle, $tag ) {
			return $script === $handle;
		} );

		// Deffer all scripts
//		$tag = preg_replace( '|<script|', '<script defer', $tag);
		return $deffered_script->isEmpty() ? $tag : preg_replace( '| src=|', ' defer src=', $tag );
	}, 999, 2 );
}
