<?php

namespace App;

use KubAT\PhpSimple\HtmlDomParser;
use Roots\Sage\Container;
use WP_Query;

/**
 * Get the sage container.
 *
 * @param null                       $abstract
 * @param array                      $parameters
 * @param \Roots\Sage\Container|null $container
 *
 * @return Container|mixed
 */
function sage( $abstract = null, $parameters = [], Container $container = null ) {
	$container = $container ? : Container::getInstance();
	if ( ! $abstract ) {
		return $container;
	}

	return $container->bound( $abstract )
		? $container->makeWith( $abstract, $parameters )
		: $container->makeWith( "sage.{$abstract}", $parameters );
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param null  $key
 * @param mixed $default
 *
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link      https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config( $key = null, $default = null ) {
	if ( is_null( $key ) ) {
		return sage( 'config' );
	}
	if ( is_array( $key ) ) {
		return sage( 'config' )->set( $key );
	}

	return sage( 'config' )->get( $key, $default );
}

/**
 * @param string $file
 * @param array  $data
 *
 * @return string
 */
function template( $file, $data = [] ) {
	return sage( 'blade' )->render( $file, $data );
}

/**
 * Retrieve path to a compiled blade view
 *
 * @param       $file
 * @param array $data
 *
 * @return string
 */
function template_path( $file, $data = [] ) {
	return sage( 'blade' )->compiledPath( $file, $data );
}

/**
 * @param $asset
 *
 * @return string
 */
function asset_path( $asset ) {
	return sage( 'assets' )->getUri( $asset );
}

/**
 * @param string|string[] $templates Possible template files
 *
 * @return array
 */
function filter_templates( $templates ) {
	$paths         = apply_filters( 'sage/filter_templates/paths', [
		'views',
		'resources/views'
	] );
	$paths_pattern = "#^(" . implode( '|', $paths ) . ")/#";

	return collect( $templates )
		->map( function ( $template ) use ( $paths_pattern ) {
			/** Remove .blade.php/.blade/.php from template names */
			$template = preg_replace( '#\.(blade\.?)?(php)?$#', '', ltrim( $template ) );

			/** Remove partial $paths from the beginning of template names */
			if ( strpos( $template, '/' ) ) {
				$template = preg_replace( $paths_pattern, '', $template );
			}

			return $template;
		} )
		->flatMap( function ( $template ) use ( $paths ) {
			return collect( $paths )
				->flatMap( function ( $path ) use ( $template ) {
					return [
						"{$path}/{$template}.blade.php",
						"{$path}/{$template}.php",
					];
				} )
				->concat( [
					"{$template}.blade.php",
					"{$template}.php",
				] );
		} )
		->filter()
		->unique()
		->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 *
 * @return string Location of the template
 */
function locate_template( $templates ) {
	return \locate_template( filter_templates( $templates ) );
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar() {
	static $display;
	isset( $display ) || $display = apply_filters( 'sage/display_sidebar', false );

	return $display;
}

/**
 * Simple function to pretty up our field partial includes.
 *
 * @param mixed $partial
 *
 * @return mixed
 */
function get_field_partial( $partial ) {
	$partial = str_replace( '.', '/', $partial );

	return include( config( 'theme.dir' ) . "/app/Fields/{$partial}.php" );
}

/**
 * Create table of contents from content provided
 *
 * @param        $content
 * @param string $list_type
 *
 * @return string
 */
function table_of_contents( $content, $list_type = 'ol' ) {
	// Get global field with selected headings
	$elements = get_field( 'toc_elements', 'option' );

	if ( ! $content || ! $elements ) {
		return '';
	}
	// Compose selector with acf values
	$elements = implode( ', ', $elements );

	$parsed_content = HtmlDomParser::str_get_html( $content );
	$headings       = $parsed_content->find( $elements );

	if ( ! $headings ) {
		return '';
	}

	// Data-toc are headings for js to select so it can create id's
	$layout = "<div class='table-of-contents' data-toc='$elements'><$list_type>";
	$HL     = 2; // Heading Level
	foreach ( $headings as $index => $heading ) {
		$current_HL = (int) substr( $heading->tag, - 1 );
		if ( $current_HL == $HL ) {
			$layout .= "<li class='$heading->tag'><a href='#heading-" . ( $index + 1 ) . "'>" . $heading->innertext() . "</a>";
		} else if ( $current_HL > $HL ) { // If bigger then last heading level, create a nested list.
			$layout .= "<$list_type><li class='$heading->tag'><a href='#heading-" . ( $index + 1 ) . "'>" . $heading->innertext() . "</a>";
		} else if ( $current_HL < $HL ) { // If less then last Heading Level, end nested list.
			for ( $i = 0; $i < ( $HL - $current_HL ); $i ++ ) {
				$layout .= "</li></$list_type></li>";
			}
			$layout .= "</li><li class='$heading->tag'><a href='#heading-" . ( $index + 1 ) . "'>" . $heading->innertext() . "</a>";
		}

		$HL = (int) substr( $heading->tag, - 1 );
	}

	$layout .= "</$list_type></div>";

	return $layout;
}

/**
 * Compose classes for product
 *
 * @param $product
 * @param $additional_classes
 *
 * @return string
 */
function product_classes( $product, $additional_classes = '' ): string {
	return 'product ' . ( $product->is_on_sale() ? 'product--onsale ' : '' ) . ( 'product--' . $product->get_type() ) . ' ' . $additional_classes;
}

/**
 * Calculate product discount in percentage
 * Not working on grouped products
 *
 * @param $product
 *
 * @return string
 */
function calculate_product_discount_percentage( $product ): string {
	if ( $product->is_type( 'grouped' ) ) {
		return '';
	}

	if ( $product->is_type( 'variable' ) ) {
		$percentages = [];

		// Get all variation prices
		$prices = $product->get_variation_prices();

		// Loop through variation prices
		foreach ( $prices[ 'price' ] as $key => $price ) {
			// Only on sale variations
			if ( $prices[ 'regular_price' ][ $key ] !== $price ) {
				// Calculate and set in the array the percentage for each variation on sale
				$percentages[] = calculate_percentage( $prices[ 'regular_price' ][ $key ], $prices[ 'sale_price' ][ $key ] );
			}
		}
		// We keep the highest value
		$percentage = max( $percentages );
	} else {
		$regular_price = (float) $product->get_regular_price();
		$sale_price    = (float) $product->get_sale_price();

		$percentage = calculate_percentage( $regular_price, $sale_price );
	}

	return $percentage === 0 ? '' : "<span class='onsale'>&minus;$percentage%</span>";
}

/**
 * @param $regular_price  float
 * @param $sale_price     float
 *
 * @return float
 */
function calculate_percentage( float $regular_price, float $sale_price ): float {
	return round( ( $regular_price - $sale_price ) / $regular_price * 100 );
}

/**
 * @param $regular_price float
 * @param $discount      float
 *
 * @return float
 */
function calculate_sale_price( float $regular_price, float $discount ): float {
	return $regular_price - $discount / 100 * $regular_price;
}

/**
 * @param string $post_type
 * @param int    $post_count
 * @param array  $taxonomies => ['taxonomy' => 'category', 'terms' => [1, 2, 3], 'field' => 'term_id']
 *
 * @param string $relation
 * @param bool   $include_current_post
 *
 * @return array|false
 */
function custom_wp_query( $post_type = 'post', $post_count = 4, $taxonomies = [], $relation = 'AND', $include_current_post = false ) {
	$tax = [];

	if ( ! empty( $taxonomies ) ) {
		$tax = [ 'relation' => $relation ];

		foreach ( $taxonomies as $taxonomy ) {
			$tax[] = [
				'taxonomy' => $taxonomy[ 'type' ],
				'terms'    => $taxonomy[ 'terms' ],
				'field'    => $taxonomy[ 'field' ]
			];
		}
	}

	$custom_posts = new WP_Query( [
		'post_type'      => $post_type,
		'posts_per_page' => $post_count,
		'no_found_rows'  => true,
		'post__not_in'   => $include_current_post ? [] : [ get_the_ID() ],
		'tax_query'      => $tax
	] );
//	wp_reset_postdata();

	if ( ! $custom_posts->have_posts() ) {
		return false;
	}

	return $custom_posts->posts;
}

/**
 * Provide only parent term id and it will find all it's children and return
 *
 * @param      $term_id
 * @param      $taxonomy
 * @param bool $hide_empty
 *
 * @return array|void
 */
function getTermChildren( $term_id, $taxonomy, $hide_empty = true ) {
	$term_arr = [];
	$terms    = get_terms( [
		'taxonomy'   => $taxonomy,
		'parent'     => $term_id,
		'hide_empty' => $hide_empty,
	] );

	if ( empty( $terms ) ) {
		return;
	}

	foreach ( $terms as $term ) {
		$term_arr[] = $term;
		$term_arr[] = getTermChildren( $term->term_id, $taxonomy );
	}

	return array_flatten( $term_arr );
}

/**
 * Get partial from views/partials/ folder and include it with wordpress function
 *
 * @param $partial
 */
function theme_get_partial( $partial ) {
	get_template_part( 'views/partials/' . $partial . '.blade' );
}

//function cleanCss( string $coverage, array $file_names ) {
//	$files = [];
//// File to clean
//	$target_css = [ 'front-page.css' ];
//// JSON coverage file to get data
//	$json_string = './Coverage-20210111T205751.json';
//
//	$obj  = json_decode( $coverage, true );
//	$path = get_theme_file_path() . '/dist/css/';
//
//	foreach ( $obj as $arr ) {
//		$url         = explode( '/', $arr[ 'url' ] );
//		$url         = array_pop( $url );
//		$is_in_array = array_search( $url, $file_names );
//
//		if ( $is_in_array === false ) {
//			continue;
//		}
//
//		$output_css       = '';
//		$cleaned_filename = 'clean-' . $file_names[ $is_in_array ];
//
//
//		foreach ( $arr[ 'ranges' ] as $name => $value ) {
//			$length     = $value[ 'end' ] - $value[ 'start' ];
//			$output_css .= substr( $arr[ 'text' ], $value[ 'start' ], $length ) . PHP_EOL;
//		}
//
////        file_put_contents( $cleaned_filename, $output_css );
//
//		$files[] = [
//			'chars'           => [
//				'old' => strlen($arr[ 'text' ]),
//				'new' => strlen( $output_css )
//			],
//			$cleaned_filename => $output_css,
//			'old_file'        => $arr[ 'text' ]
//		];
//	}
//
//	return $files;
//}

/**
 * @param        $text
 * @param int    $max_length
 * @param string $cut_off
 * @param false  $keep_word
 *
 * @return false|mixed|string
 */
function shorten_text( $text, $max_length = 140, $cut_off = '...', $keep_word = false ) {
	if ( strlen( $text ) <= $max_length ) {
		return $text;
	}

	if ( strlen( $text ) > $max_length ) {
		if ( $keep_word ) {
			$text = substr( $text, 0, $max_length + 1 );

			if ( $last_space = strrpos( $text, ' ' ) ) {
				$text = substr( $text, 0, $last_space );
				$text = rtrim( $text );
				$text .= $cut_off;
			}
		} else {
			$text = substr( $text, 0, $max_length );
			$text = rtrim( $text );
			$text .= $cut_off;
		}
	}

	return $text;
}
