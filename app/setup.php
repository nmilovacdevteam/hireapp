<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Theme assets
 */
add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_script( 'main.js', asset_path( 'scripts/main.js' ), [ 'jquery' ], null, true );

	$css_file = 'styles/';

	switch ( true ) {
		case is_front_page():
			$css_file .= 'front-page.css';
			wp_dequeue_script( 'main.js' );
			wp_enqueue_script( 'front-page.js', asset_path( 'scripts/front-page.js' ), [ 'jquery' ], null, true );
			break;
		case is_page_template( 'views/about.blade.php' ):
			$css_file .= 'about.css';
			break;
		case is_page_template( 'views/toc.blade.php' ):
			$css_file .= 'toc.css';
			wp_dequeue_script( 'main.js' );
			wp_enqueue_script( 'toc.js', asset_path( 'scripts/toc.js' ), [ 'jquery' ], null, true );
			break;
		case is_page_template( 'views/faq.blade.php' ):
			$css_file .= 'faq.css';
			wp_dequeue_script( 'main.js' );
			wp_enqueue_script( 'faq.js', asset_path( 'scripts/faq.js' ), [ 'jquery' ], null, true );
			break;
		case is_page_template( 'views/careers.blade.php' ):
			$css_file .= 'careers.css';
			wp_dequeue_script( 'main.js' );
			wp_enqueue_script( 'careers.js', asset_path( 'scripts/careers.js' ), [ 'jquery' ], null, true );
			break;
		case is_page_template( 'views/workers.blade.php' ):
			$css_file .= 'workers.css';
			wp_dequeue_script( 'main.js' );
			wp_enqueue_script( 'workers.js', asset_path( 'scripts/workers.js' ), [ 'jquery' ], null, true );
			break;
		case is_page_template( 'views/business.blade.php' ):
			$css_file .= 'business.css';
			break;
		case is_archive() || is_home():
			$css_file .= 'blog.css';
			wp_dequeue_script( 'main.js' );
			wp_enqueue_script( 'blog.js', asset_path( 'scripts/blog.js' ), [ 'jquery' ], null, true );
			break;
		case is_single():
			$css_file .= 'single.css';
			break;
		default:
			$css_file .= 'main.css';
	}

	$main_css_path = asset_path( $css_file );
	echo "<link rel='preload' href='$main_css_path' as='style'><link rel='stylesheet' href='$main_css_path'>";

	/**
	 * Dequeue unnecessary styles and scripts
	 */
	wp_dequeue_style( 'woocommerce-inline' );
	wp_dequeue_style( 'wc-block-style' );
	wp_deregister_script( 'wc-cart-fragments' );

	wp_deregister_script( 'wc-add-to-cart' );
	wp_deregister_script( 'wc-add-to-cart-variation' );

}, 100 );

/**
 * Theme setup
 */
add_action( 'after_setup_theme', function () {
	/**
	 * Enter theme text domain
	 */
	define( 'THEME_TEXT_DOMAIN', 'hireapp' );


	/**
	 * Custom shortcodes
	 */
	add_shortcode( 'youtube', 'youtube_embed' );


	/**
	 * Enable features from Soil when plugin is activated
	 * @link https://roots.io/plugins/soil/
	 */
	add_theme_support( 'soil', [
		'clean-up',
		//        'disable-rest-api',
		'disable-asset-versioning',
		'disable-trackbacks',
		//        'google-analytics' => 'UA-XXXXX-Y',
		'js-to-footer',
		//		'nav-walker',
		'nice-search',
		//        'relative-urls'
	] );

	/**
	 * Enable plugins to manage the document title
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
	 */
	add_theme_support( 'title-tag' );

	/**
	 * Register navigation menus
	 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
	 */
	register_nav_menus( [
		'primary_navigation'   => __( 'Glavna navigacija', THEME_TEXT_DOMAIN ),
		'secondary_navigation' => __( 'Footer navigacija', THEME_TEXT_DOMAIN ),
		'third_navigation'     => __( 'TOC & Privacy navigacija', THEME_TEXT_DOMAIN )
	] );

	/**
	 * Enable post thumbnails
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Enable HTML5 markup support
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
	 */
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	/**
	 * Enable selective refresh for widgets in customizer
	 * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Use main stylesheet for visual editor
	 * @see resources/assets/styles/layouts/_tinymce.scss
	 */
	//add_editor_style( asset_path( 'styles/main.css' ) );

	/**
	 * Defining image sizes globally
	 */
	add_image_size( 'about_fold', 619, 468, true );
	add_image_size( 'members', 305, 318, true );
	add_image_size( 'blog', 630, 400, true );
	add_image_size( 'single_blog', 1143, 479, true );
	add_image_size( 'small_single_blog', 169, 126, true );
	add_image_size( 'team_featured', 1064, 304, true );
	add_image_size( 'front_fold', 787, 605, true );
	add_image_size( 'logos', 56, 69, true );
	add_image_size( 'stories', 522, 466, true );
	add_image_size( 'hire', 1440, 807, true );
	add_image_size( 'faq', 518, 573, true );
	add_image_size( 'logos_workers', 138, 50, true );
	add_image_size( 'works', 367, 700, true );
}, 20 );

/*
 * Add custom code to head
 */
add_action( 'wp_head', function () {
	// Head options code
	echo get_field( 'insert_head', 'option' ) ?: '';

	$site_url     = site_url();
	$favicon_path = asset_path( 'images/favicon/' );
	// Font preloading, preload only necessary fonts
	$fonts = [
		asset_path( 'fonts/Wotfard-Regular.woff2' ),
		asset_path( 'fonts/Wotfard-Bold.woff2' ),
		asset_path( 'fonts/AvenirLTStd-Book.woff2' ),
		asset_path( 'fonts/AvenirLTStd-Black.woff2' ),
		asset_path( 'fonts/AvenirLTStd-Roman.woff2' ),
	];
	foreach ( $fonts as $font ) {
		echo "<link rel='preload' href='$font' as='font' type='font/woff2' crossorigin>";
	}
	?>
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <link rel='dns-prefetch' href='<?php echo $site_url; ?>'/>
    <link rel='dns-prefetch' href='//code.jquery.com' crossorigin="anonymous"/>
    <link rel='dns-prefetch' href='//s.w.org' crossorigin="anonymous"/>

    <link rel="shortcut icon" href="<?php echo $favicon_path; ?>/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $favicon_path; ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $favicon_path; ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $favicon_path; ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $favicon_path; ?>/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $favicon_path; ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
	<?php
} );

/**
 * Add custom code to footer
 */
add_action( 'wp_footer', function () {
	// Footer options code
	echo get_field( 'insert_footer', 'option' ) ?: '';
} );

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action( 'the_post', function ( $post ) {
	try {
		sage( 'blade' )->share( 'post', $post );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:223: %s', THEME_TEXT_DOMAIN ), $e );
	}
} );

/**
 * Setup Sage options
 */
add_action( 'after_setup_theme', function () {
	/**
	 * Add JsonManifest to Sage container
	 */
	try {
		sage()->singleton( 'sage.assets', function () {
			return new JsonManifest( config( 'assets.manifest' ), config( 'assets.uri' ) );
		} );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:238: %s', THEME_TEXT_DOMAIN ), $e );
	}

	/**
	 * Add Blade to Sage container
	 */
	try {
		sage()->singleton( 'sage.blade', function ( Container $app ) {
			$cachePath = config( 'view.compiled' );
			if ( ! file_exists( $cachePath ) ) {
				wp_mkdir_p( $cachePath );
			}
			( new BladeProvider( $app ) )->register();

			return new Blade( $app['view'] );
		} );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:248: %s', THEME_TEXT_DOMAIN ), $e );
	}

	/**
	 * Create @asset() Blade directive
	 */
	try {
		sage( 'blade' )->compiler()->directive( 'asset', function ( $asset ) {
			return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
		} );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:264: %s', THEME_TEXT_DOMAIN ), $e );
	}
} );

/**
 * Initialize ACF Builder
 */
add_action( 'init', function () {
	// Remove unwanted image sizes
	remove_image_size( '1536x1536' );
	remove_image_size( '2048x2048' );
	remove_image_size( 'woocommerce_gallery_thumbnail' );
	remove_image_size( 'shop_thumbnail' );
	remove_image_size( 'shop_catalog' );

	if ( function_exists( 'acf_add_local_field_group' ) ) {
		try {
			collect( glob( config( 'theme.dir' ) . '/app/Fields/*.php' ) )->map( function ( $field ) {
				return require_once( $field );
			} )->map( function ( $field ) {
				if ( $field instanceof FieldsBuilder ) {
					acf_add_local_field_group( $field->build() );
				}
			} );
		} catch ( \Exception $e ) {
			printf( __( 'Došlo je do greške u inicijalizaciji ACF Builder-a (app/setup.php:274): %s', THEME_TEXT_DOMAIN ), $e );
		}
	}
} );
