import './modules/global';
import CreateCustomSelect from './modules/custom-select';

window.addEventListener('load', () => {
    new CreateCustomSelect('.category_picker_select');
    const load_more_btn = document.querySelector('.btn--load'),
        stories_container = document.querySelector('.posts_output');

    load_more_btn && load_more_btn.addEventListener('click', () => {
        const {page, tax, post_type, term, max_pages, offset, posts_page} = load_more_btn.dataset;

        loadStories(page, tax, post_type, term, stories_container, offset, posts_page);

        if (max_pages == page) {
            load_more_btn.remove();
        }

        load_more_btn.dataset.page = +page + 1;
    });

    document.addEventListener('click', e => {
        if (!e.target.closest('.category_picker a')) return;
        e.preventDefault();

        const term = e.target.dataset.category;
        document.body.classList.add('active');

        document.querySelectorAll('.category_picker a').forEach(function (el, index) {
            el.classList.remove('selected');
        })

        e.target.classList.add('selected');


        document.querySelector('.btn--load').dataset.term = e.target.dataset.category;

        loadBlogs(term);
    });

    document.addEventListener('change', e => {
        if (!e.target.closest('.category_picker_select')) return;
        e.preventDefault();

        const term = e.target.value;
        document.body.classList.add('active');


        document.querySelector('.btn--load').dataset.term = e.target.value;

        loadBlogs(term);
    });
});

function loadStories(page, taxonomy, post_type, term, output, offset, posts_page) {
    document.body.classList.add('loading');
    fetch(`/wp-json/custom/load-posts?page=${page}&term=${term}&taxonomy=${taxonomy}&post_type=${post_type}&offset=${offset}&posts_page=${posts_page}`)
        .then(res => res.json())
        .then(data => {
            document.body.classList.remove('loading');

            if (data.error) {
                console.error(data.error);
                return;
            }

            output.insertAdjacentHTML('beforeend', data.posts);
        });
}

function loadBlogs(term) {
    document.body.classList.add('loading');
    const posts_container = document.querySelector('.posts_output');
    fetch(`/wp-json/custom/load-category-posts?term=${term}`)
        .then(res => res.json())
        .then(data => {
            document.body.classList.remove('loading');

            if (data.error) {
                console.error(data.error);
                return;
            }

            posts_container.innerHTML = data.posts;
        });
}
