import './modules/global';
import CreateCustomSelect from './modules/custom-select';

window.addEventListener('load', () => {
    new CreateCustomSelect('.openings_picker_select');

    document.addEventListener('change', e => {
        if (!e.target.closest('.openings_picker_select')) return;
        e.preventDefault();

        const term = e.target.value;
        document.body.classList.add('active');

        document.querySelector('.btn--load').dataset.term = e.target.value;

        loadOpenings(term);
    });

    document.addEventListener('click', e => {
        if (!e.target.closest('.openings_picker a')) return;
        e.preventDefault();

        const term = e.target.dataset.opening;
        document.body.classList.add('active');

        document.querySelectorAll('.openings_picker a').forEach(function (el, index) {
            el.classList.remove('selected');
        })

        e.target.classList.add('selected');


        loadOpenings(term);
    });

    document.addEventListener('click', e => {
        if (!e.target.closest('.apply a')) return;

       document.querySelector('#apply h3').textContent = 'Submit an application For ' + e.target.dataset.title;
       document.querySelector('#apply .hidden').value = e.target.dataset.title;
    })
});

function loadOpenings(term) {
    document.body.classList.add('loading');
    const openings_container = document.querySelector('.listing_openings');
    fetch(`/wp-json/custom/load-openings-posts?term=${term}`)
        .then(res => res.json())
        .then(data => {
            document.body.classList.remove('loading');

            if (data.error) {
                console.error(data.error);
                return;
            }

            openings_container.innerHTML = data.openings;
        });
}
