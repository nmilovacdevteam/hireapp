import './modules/global';

window.addEventListener('load', () => {
    document.addEventListener('click', function (e) {
        if (!e.target.closest('.single_selection-wrapper')) return;

        e.preventDefault();

        document.querySelectorAll('.single_selection-wrapper').forEach(function (el, index) {
            el.classList.remove('active');
        })

        e.target.classList.add('active');

        document.querySelectorAll('.showed').forEach(function (el, index) {
            el.classList.remove('showed');
        })

        document.querySelectorAll(`.${e.target.dataset.target}`).forEach(function (el, index) {
            el.classList.add('showed');
        })
    })

    document.addEventListener('keypress', function (e) {
        if (!e.target.closest('#search')) return;
        let search = e.target.value;

        setTimeout(() => {
            loadFAQ(search);
        }, 500);
    })
});

function loadFAQ(search) {
    const faq_container = document.querySelector('#accordion');
    fetch(`/wp-json/custom/load-faq?search=${search}`)
        .then(res => res.json())
        .then(data => {
            document.body.classList.remove('loading');

            if (data.error) {
                console.error(data.error);
                return;
            }

            faq_container.innerHTML = data.faqs;
        });
}
