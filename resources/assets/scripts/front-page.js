import './modules/global';
import Glide from '@glidejs/glide';

window.addEventListener('load', () => {
    const slider_elements = document.querySelectorAll('.glide');

    slider_elements.length && slider_elements.forEach(slider => {
        if (slider.classList.contains('default__glide')) {
            const options = JSON.parse(slider.dataset.options);
            new Glide(`#${slider.id}`, options).mount();
        }
    });
});
