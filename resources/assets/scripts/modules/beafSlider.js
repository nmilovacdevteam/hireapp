/**
 * Beaf Slider - JS class for creating slider with before and after images
 *
 *
 * Template:
 <div id="slider-1" class="beaf-slider">
 <picture class="project__image project__image--after">
 <img src="" data-srcset="" alt="">
 </picture>
 <picture class="project__image project__image--before">
 <img src="" data-srcset="" alt="">
 </picture>
 </div>
 */
export default class BeafSlider {
	constructor( sliders = document.querySelectorAll( '.beaf-slider' ) ) {
		this.sliders = sliders;

		sliders.length && sliders.forEach( slider => {
			// Add visual control with thumb
			slider.insertAdjacentHTML( 'beforeend', this.sliderControlTemplate() );

			// Add range for controlling
			this.addRange( slider );


			//
			const btn_container   = slider.querySelector( '.beaf-slider--btn-holder' ),
			      image_container = slider.querySelector( '.beaf-slider__image--before' ),
			      image           = image_container.querySelector( 'img' ),
			      el_position     = slider.getBoundingClientRect().width / 2;

			image.style.width             = slider.offsetWidth + 'px';
			image_container.style.width   = el_position + 'px';
			btn_container.style.transform = `translate3d(${ el_position }px, 0, 0)`;
		} );
	}

	moveSlider( slider_container, range ) {
		const btn_container = slider_container.querySelector( '.beaf-slider--btn-holder' ),
		      image         = slider_container.querySelector( '.beaf-slider__image--before' );

		image.style.width             = range.value + '%';
		btn_container.style.transform = `translate3d(${ slider_container.offsetWidth * range.value / 100 }px, 0, 0)`;
	}

	sliderControlTemplate() {
		return `<div class="beaf-slider--btn-holder">
                <button type="button" class="btn beaf-slider--thumb">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="19" viewBox="0 0 21 13">
                        <path d="M8,6.5v5.4c0,0.7-0.8,1-1.3,0.6L4,10.1l-3.3-3c-0.3-0.3-0.3-0.9,0-1.2l3.3-3l2.7-2.4C7.2,0.1,8,0.4,8,1.1V6.5z M13,11.9
                        c0,0.7,0.8,1,1.3,0.6l2.7-2.4l3.3-3c0.3-0.3,0.3-0.9,0-1.2l-3.3-3l-2.7-2.4C13.8,0.1,13,0.4,13,1.1v5.4V11.9z" />
                    </svg>
                </button>
            </div>`;
	}

	addRange( slider ) {
		const input_range = document.createElement( 'input' );
		input_range.type  = 'range';
		input_range.min   = '0%';
		input_range.value = '50%';
		input_range.max   = '100%';
		input_range.id    = 'range-' + slider.id;
		input_range.classList.add( 'beaf-slider__range' );

		slider.appendChild( input_range );
	}

	init() {
		document.addEventListener( 'input', e => {
			if ( !e.target.closest( '.beaf-slider__range' ) ) return;

			const closest_el       = e.target.closest( '.beaf-slider__range' ),
			      slider_container = closest_el.closest( '.beaf-slider' );

			this.moveSlider( slider_container, closest_el );
		} );

		// Prevent input from changing on mouse wheel
		document.addEventListener( 'wheel', e => {
			if ( document.activeElement.type === 'range' ) {
				document.activeElement.blur();
			}
		} )
	}
}
