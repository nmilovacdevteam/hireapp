import { Emmit } from './utils';

/**
 * Initialize load more, attaches click event to button with load more function
 *
 * @param callback {Boolean | Function}
 *
 * @param errorCallback {Boolean | Function}
 */
export function initFilterPosts( callback = false, errorCallback = false ) {
	const filters_container = document.querySelector( '[data-post_type]' ),
	      inputs            = filters_container.querySelectorAll( '.filters input' );

	document.addEventListener( 'change', e => {
		if ( !e.target.closest( '.filters input' ) ) return;

		const inputValues = getInputCheckValues( inputs ),
		      params      = {
			      post_type: filters_container.dataset.post_type,
			      taxonomy : []
		      };

		inputValues.forEach( value => {
			params[ 'taxonomy' ].push( value[ 0 ] );
			params[ `taxonomy-${ value[ 0 ] }` ] = value[ 1 ];
		} );

		loadPosts( composeParameters( params ), callback, errorCallback );
	} );
}

/**
 * Object with parameters
 * {parameter_name: parameter_value, parameter_name_2: parameter_value_2}
 * @param params
 * @returns {string}
 */
function composeParameters( params = {} ) {
	return Object.entries( params ).map( parameter => {
		return `${ parameter[ 0 ] }=${ parameter[ 1 ] }&`;
	} ).join( '' );
}

/**
 *
 * @param inputs
 * @returns [string, [array]]
 */
function getInputCheckValues( inputs ) {
	const obj = {};

	Array.from( inputs ).filter( input => input.checked ).forEach( input => {
		if ( obj[ input.name ] ) {
			obj[ input.name ].push( input.value );
		} else {
			obj[ input.name ] = [ input.value ];
		}
	} )

	return Object.entries( obj );
}

/**
 * Function to get posts with REST API, includes function for rendering HTML
 *
 * @param parameters
 * @param callback {Boolean | Function}
 *
 * @param errorCallback {Boolean | Function}
 * @emits fetch.started and fetch.finished
 */
function loadPosts( parameters, callback, errorCallback ) {
	// Add class on body for animations
	document.body.classList.add( 'loading' );

	// Emmit event for fetch start
	new Emmit( '', { type: 'fetch.started' } );

	fetch( `/wp-json/custom/load_posts?${ parameters }` )
		.then( res => res.json() )
		.then( data => {
			// Remove class from body and stop animations
			document.body.classList.remove( 'loading' );

			// Emmit event for fetch end
			new Emmit( '', { type: 'fetch.finished' } );

			// Check if there has been an error in backend
			if ( data.error ) {
				// If there is error callback call it
				if ( errorCallback && typeof errorCallback === 'function' ) {
					// Compose posts layout
					errorCallback( data.error );
				}

				console.error( 'Dogodila se greška: ', data.error );
				return;
			}

			if ( callback && typeof callback === 'function' ) {
				// Compose posts layout
				callback( data.data );
			}

		} )
		.catch( error => {
			// Emmit event for fetch error
			new Emmit( '', { type: 'fetch.error' } );

			console.error( error );
		} );
}
