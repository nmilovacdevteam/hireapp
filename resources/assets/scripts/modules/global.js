import './header';
import Glide                                                                   from '@glidejs/glide';
import { Cookie }                                                              from './cookie';
import { ShowElement, HideElement, ToggleElement, RemoveAutoFill, TOC, ToTop, Collapsible } from './utils';
import { deffer_images }                                                       from './img-defer';
import Carousel                                                                from 'bootstrap/js/src/carousel';
// import smoothscroll                                                from 'smoothscroll-polyfill';

window.addEventListener( 'load', () => {
	// smoothscroll.polyfill();
	deffer_images( {} );

	new Collapsible();

	document.querySelectorAll( '.carousel' ).forEach( carousel => {
		new Carousel( carousel );
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '[data-open]' ) ) return;
		ShowElement( e.target.closest( '[data-open]' ) );
	} );

	document.addEventListener( 'click', e => {
		const openned_modal = document.querySelector( '.modal.show' );
		if ( e.target.closest( '[data-close]' ) ) {
			HideElement( e.target.closest( '[data-close]' ) );
		} else if ( e.target == openned_modal ) {
			HideElement( openned_modal );
		}
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '[data-togle]' ) ) return;
		ToggleElement( e.target.closest( '[data-togle]' ) );
	} );

	new RemoveAutoFill( document.querySelectorAll( 'form' ) );

	// Init Table of Contents
	new TOC( {} );

	new ToTop( '.btn--top' );

	const slider_elements = document.querySelectorAll( '.glide' );

	slider_elements.length && slider_elements.forEach( slider => {
		const options = JSON.parse( slider.dataset.options );

		new Glide( `#${ slider.id }`, options ).mount();
	} );


	new Cookie( { cookieContainer: '.cookie', cookieName: 'policy', cookieValue: 'accepted', cookieExpire: 30 } ).init();
} );
