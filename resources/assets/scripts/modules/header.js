import { HeightAnim } from './utils';

window.addEventListener( 'load', () => {
	// Control dropdown's on mobile
	if ( window.matchMedia( '(max-width: 991px)' ).matches ) {
		document.addEventListener( 'click', e => {
			if ( !e.target.closest( '#main-navigation .menu-item-has-children' ) && !e.target.closest( '.toggler--navbar' ) ) return;
			const closest_element = e.target.closest( '#main-navigation .menu-item-has-children' ) || e.target.closest( '.toggler--navbar' ),
			      navbar          = closest_element.dataset.togle ? document.querySelector( closest_element.dataset.togle + ' .navbar-nav' ) : closest_element.parentElement,
			      items           = Array.from( navbar.children );
			items.forEach( item => {
				const sub_menu = item.querySelector( '.sub-menu' );

				if ( item.classList === closest_element.classList ) {
					closest_element.classList.toggle( 'open' );
					sub_menu && sub_menu.classList.toggle( 'open' );
				} else {
					item.classList.remove( 'open' );
					sub_menu && sub_menu.classList.remove( 'open' );
				}

				sub_menu && new HeightAnim( sub_menu, 'open' );
			} );
		} );
	}
} );
