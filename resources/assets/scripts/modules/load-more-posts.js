/**
 * Async function for getting posts
 *
 * @returns {Promise<any>}
 * @param parameters
 */
async function getPosts( parameters ) {
	// Get current language code of the page
	const language = document.documentElement.lang.split( '-' )[ 0 ];
	let response   = await fetch( `${ language != 'sr' ? `/${ language }` : '' }/wp-json/custom/load_posts?${ parameters }` );
	return await response.json();
}

/**
 * Function to render HTML for posts page
 *
 * @param posts
 * @param insert_nto
 */
function renderPostsLayout( posts = [], insert_nto = document.querySelector( '.posts' ) ) {
	let html_layout = '';

	posts.forEach( post => {
		const { image, link, title, date } = post;
		const post_image                   = image ? `<div class="post__image"><picture>${ image }</picture></div>` : '';

		html_layout += `
        <article class="post post--card">
			<a href="${ link }" class="post__image">
				${ post_image }
			</a>
            <p class="post__date">${ date }</p>
            <h2 class="post__title">
                <a href="${ link }">${ title }</a>
            </h2>
		</article>
        `;
	} );
	insert_nto.insertAdjacentHTML( 'beforeend', html_layout );
}

/**
 * Initialize load more, attaches click event to button with load more function
 *
 * @param btn
 */
export function initLoadMorePosts( btn = Element ) {
	let current_page = 2;
	const params     = {
		page:     current_page,
		category: btn.dataset.category || ''
	};

	loadPosts( btn, composeParameters( params ) );

	btn.addEventListener( 'click', function () {
		params.page++;
		loadPosts( this, composeParameters( params ) );
	} );
}

/**
 * Object with parameters
 * {parameter_name: parameter_value, parameter_name_2: parameter_value_2}
 * @param params
 * @returns {string}
 */
function composeParameters( params = {} ) {
	return Object.entries( params ).map( parameter => {
		return `${ parameter[ 0 ] }=${ parameter[ 1 ] }&`;
	} ).join( '' );
}

/**
 * Function to get posts with REST API, includes function for rendering HTML
 *
 * @param btn {{prototype: Element; new(): Element}}
 * @param parameters
 */
function loadPosts( btn, parameters ) {
	btn.classList.add( 'loading' );
	getPosts( parameters )
		.then( data => {
			// Stop animation after request has been made
			btn.classList.remove( 'loading' );

			// Check if there has been an error in backend
			if ( data.error ) {
				console.error( 'Dogodila se greška: ', data.error );
				return;
			}

			// Compose posts layout
			renderPostsLayout( data.data );

			// Remove load more button if it is last page
			if ( data.last_page ) {
				btn.parentElement.remove();
			}
		} )
		.catch( error => {
			btn.classList.remove( 'loading' );
			console.error( error );
		} );
}
