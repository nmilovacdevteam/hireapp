async function fetchAndRead( rest_url ) {
	const response = await fetch( rest_url );

	return await getTextFromStream( response.body );
}

async function getTextFromStream( readableStream ) {
	const reader      = readableStream.getReader(),
	      utf8Decoder = new TextDecoder();
	let nextChunk;

	let resultStr = '';

	while ( !( nextChunk = await reader.read() ).done ) {
		let partialData = nextChunk.value;
		resultStr += utf8Decoder.decode( partialData );
	}

	return resultStr;
}

export function QuickView( outputElement, product_id ) {
	const rest_url       = '/wp-json/custom/product?product_id=',
	      fetchFinish    = document.createEvent( 'HTMLEvents' ),
	      loaded_product = outputElement.dataset.product_loaded;

	document.body.classList.add( 'loading' );

	if ( loaded_product == product_id ) {
		document.body.classList.remove( 'loading' );
		outputElement.parentElement.parentElement.classList.add( 'show' );
		return;
	}

	loadProduct( outputElement, rest_url + product_id, fetchFinish, product_id );
}

function loadProduct( outputElement, rest_url, customEvent, product_id ) {
	fetchAndRead( rest_url )
		.then( data => {
			outputElement.innerHTML              = data;
			outputElement.dataset.product_loaded = product_id;

			// Display product modal
			outputElement.parentElement.parentElement.classList.add( 'show' );
			// Prevent scrolling while modal is openned
			document.body.classList.remove( 'loading' );
			document.body.classList.add( 'overflow' );

			customEvent.initEvent( 'fetchDone', true, false );
			document.dispatchEvent( customEvent );
		} )
		.catch( error => {
			console.error( error );
		} );
}