/**
 * Returns random number from provided min and max values
 * @param min {number}
 * @param max {number}
 * @returns {number}
 */
function RandomNumbers( min, max ) {
	min = Math.ceil( min );
	max = Math.floor( max );
	return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
}

/**
 * Showing element with css class or jquery methods fadeIn and slideIn
 *
 * @param element {Element}             HTML Element from Document
 * @param InputToFocus {Element|String} Pass HTML element or selector
 * @param elClass {string}              Class to control, default is show
 */
function ShowElement( element, InputToFocus = '', elClass = 'show' ) {
	element.setAttribute( 'aria-expanded', 'true' );
// Get element to open from btn's data-open attribute
	const target_element    = document.querySelector( element.dataset.open ),
	      type_of_animation = element.dataset.hasOwnProperty( 'function' ) ? element.dataset.function : null;

	if ( target_element == undefined ) {
		console.warn( 'Cannot find target element: ', target_element );
		return;
	}

	// If element to open has class modal, add class overflow to body
	// Overflow class is used in css to apply overflow hidden on body
	// to prevent scrolling while modal is openned
	if ( target_element.classList.contains( 'modal' ) || target_element.classList.contains( 'navbar-collapse' ) ) {
		document.body.classList.add( 'overflow' );
	}
	switch ( type_of_animation ) {
		case 'fade':
			$( target_element ).fadeIn();
			break;
		case 'slide':
			$( target_element ).slideDown();
			break;
		default:
			target_element.classList.add( elClass );
	}

	// Focus on input after openning modal if provided
	if ( InputToFocus ) {
		const input_el = InputToFocus instanceof HTMLElement ? InputToFocus : target_element.querySelector( InputToFocus );
		input_el && input_el.focus();
	}
	window.toggled_btn && window.toggled_btn.setAttribute( 'aria-expanded', 'false' );
	window.toggled_btn = element;
	// window.showed_item && window.showed_item.classList.remove( elClass );
	// window.showed_item = target_element;
	new Emmit( target_element, { type: 'show' } );
}

/**
 * Hiding element with css class or jquery methods fadeIn and slideIn
 *
 * @param element {Element}     HTML Element from Document
 * @param elClass {string}      Class to control, default is show
 */
function HideElement( element, elClass = 'show' ) {
	element.setAttribute( 'aria-expanded', 'false' );
	// Get element to open from btn's data-open attribute
	const target_element    = element.classList.contains( 'modal' ) ? element : document.querySelector( element.dataset.close ),
	      type_of_animation = element.dataset.hasOwnProperty( 'function' ) ? element.dataset.function : null;

	if ( target_element == undefined ) {
		console.warn( 'Cannot find target element: ', target_element );
		return;
	}

	// If element to open has class modal, add class overflow to body
	// Overflow class is used in css to apply overflow hidden on body
	// to prevent scrolling while modal is openned
	if ( target_element.classList.contains( 'modal' ) || target_element.classList.contains( 'navbar-collapse' ) ) {
		document.body.classList.remove( 'overflow' );
	}
	switch ( type_of_animation ) {
		case 'fade':
			$( target_element ).fadeOut();
			break;
		case 'slide':
			$( target_element ).slideUp();
			break;
		default:
			target_element.classList.remove( elClass );
	}
	window.toggled_btn && window.toggled_btn.setAttribute( 'aria-expanded', 'false' );
	window.toggled_btn = undefined;
	// window.showed_item && window.showed_item.classList.remove( elClass );
	// window.showed_item = undefined;
	new Emmit( target_element, { type: 'hide' } );
}

/**
 * Toggles element with css class or jquery methods fadeIn and slideIn
 *
 * @param element {Element}     HTML Element from Document
 * @param elClass {string}      Class to control, default is show
 */
function ToggleElement( element, elClass = 'show' ) {
	element.setAttribute( 'aria-expanded', element.getAttribute( 'aria-expanded' ) === 'true' ? 'false' : 'true' );
	// Get element to open from btn's data-open attribute
	const target_element    = document.querySelector( element.dataset.togle ),
	      type_of_animation = element.dataset.hasOwnProperty( 'function' ) ? element.dataset.function : null;

	if ( target_element == undefined ) {
		console.warn( 'Cannot find target element: ', target_element );
		return;
	}

	// If element to open has class modal, add class overflow to body
	// Overflow class is used in css to apply overflow hidden on body
	// to prevent scrolling while modal is openned
	if ( target_element.classList.contains( 'modal' ) || target_element.classList.contains( 'navbar-collapse' ) ) {
		document.body.classList.toggle( 'overflow' );
	}
	switch ( type_of_animation ) {
		case 'fade':
			$( target_element ).fadeToggle();
			break;
		case 'slide':
			$( target_element ).slideToggle();
			break;
		default:
			target_element.classList.toggle( elClass );
	}

	window.toggled_btn && window.toggled_btn.toggleAttribute( 'aria-expanded' );
	window.toggled_btn = window.toggled_btn ? undefined : element;
	// window.showed_item && window.showed_item.classList.toggle( elClass );
	// window.showed_item = window.showed_item ? undefined : target_element;
	new Emmit( target_element, { type: 'toggle' } );
}

/**
 * Custom collapse function. Closes all siblings when one is clicked and toggles clicked one if necessery
 * Layout for Collapse
 *
 <div id="accordion">
 <div class="collapsible">
 <button class="btn active" data-collapse="#collapseOne" data-parent="#accordion">Element 1</button>
 <div id="collapseOne" class="collapse show">

 </div>
 </div>
 <div class="collapsible">
 <button class="btn" data-collapse="#collapseTwo" data-parent="#accordion">Element 2</button>
 <div id="collapseTwo" class="collapse">

 </div>
 </div>
 </div>
 *
 */
function Collapsible() {
	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '[data-collapse]' ) ) return;
		const closest_el        = e.target.closest( '[data-collapse]' ),
		      parent_el         = document.querySelector( closest_el.dataset.parent ),
		      current_target_el = parent_el.querySelector( closest_el.dataset.collapse ),
		      btn_collapses     = parent_el.querySelectorAll( '[data-collapse]' );

		btn_collapses.forEach( btn => {
			const other_target_el = parent_el.querySelector( btn.dataset.collapse );

			// If it's the same btn than toggle elements class, otherwise remove class show
			if ( btn.classList === closest_el.classList ) {
				current_target_el.classList.toggle( 'show' );
				closest_el.classList.toggle( 'active' );
			} else {
				other_target_el.classList.remove( 'show' );
				btn.classList.remove( 'active' );
			}

			// Control every collapsible height
			new HeightAnim( other_target_el );
		} );

	} );
}

/**
 * Scroll element up or down based on attributes
 *
 * @param item_to_scroll
 * @param direction
 */
function ScrollItem( item_to_scroll, direction ) {
	let amount_to_scroll_top  = item_to_scroll.scrollTop,
	    amount_to_scroll_left = item_to_scroll.scrollLeft;

	switch ( direction ) {
		case 'up':
			amount_to_scroll_top += 100;
			break;
		case 'down':
			amount_to_scroll_top -= 100;
			break;
		case 'left':
			amount_to_scroll_left -= 100;
			break;
		case 'right':
			amount_to_scroll_left += 100;
			break;
		default:
			amount_to_scroll_left += 100;
	}

	item_to_scroll.scroll( {
		                       top:      amount_to_scroll_top,
		                       left:     amount_to_scroll_left,
		                       behavior: 'smooth'
	                       } );
}

/**
 * Add class on header when document is scrolled pass header height.
 * You can pass optional callback function that receives header element as parameter
 *
 * @param header {Element | string}
 * @param callback {Function | boolean}
 */
function HeaderControl( header, callback = false ) {
	const header_el     = header instanceof Element ? header : document.querySelector( header ),
	      header_height = header_el.offsetHeight;

	if ( window.scrollY >= header_height ) {
		header_el.classList.add( 'scrolled' );
	} else {
		header_el.classList.remove( 'scrolled' );
	}

	if ( callback ) {
		callback( header_el );
	}

	window.requestAnimationFrame( () => {
		HeaderControl( header_el, callback );
	} );
}

/**
 * Animate elements height to original
 * @param element
 * @param classCheck {string}
 * @constructor
 */
function HeightAnim( element, classCheck = 'show' ) {
	element.style.height = 'auto';
	const item_height    = element.classList.contains( classCheck ) ? element.offsetHeight : 0;
	element.style.height = '0px';
	setTimeout( () => {
		element.style.height = item_height + 'px';
	}, 50 );
}

/**
 *
 * @param elementToCheck {Element}
 * @param elementToAnimate {Element}
 * @param classCheck
 * @param minHeight {number}
 * @constructor
 */
function HeightAnim_2( elementToCheck, elementToAnimate, minHeight = 0, classCheck = 'show' ) {
	elementToAnimate.style.height = 'auto';
	const item_height             = elementToCheck.classList.contains( classCheck ) ? elementToAnimate.offsetHeight : minHeight;
	elementToAnimate.style.height = minHeight + 'px';
	setTimeout( () => {
		elementToAnimate.style.height = item_height + 'px';
	}, 50 );
}

/**
 * Custom event delegation like jQuery on method
 *
 * @param event
 * @param element
 * @param listener
 * @constructor
 */
function Delegate( event, element, listener ) {
	document.addEventListener( event, e => {
		if ( !e.target.closest( element ) ) return;
		listener();
	} );
}

/**
 * Remove autofill from forms on page
 * @param forms
 * @constructor
 */
function RemoveAutoFill( forms ) {
	if ( !forms ) {
		return;
	}

	if ( forms instanceof HTMLElement ) {
		forms.setAttribute( 'autocomplete', 'off' );
		return;
	}

	forms.forEach( form => form.setAttribute( 'autocomplete', 'off' ) );
}

/**
 * Create form data from form elements
 * @param form_elements
 * @returns {FormData}
 * @constructor
 */
function CreateFormData( form_elements ) {
	// Make form data object and populate it with all inputs from form
	let data = new FormData();
	Array.from( form_elements ).forEach( el => {
		if ( el.type === 'file' ) {
			data.append( el.name, el.files[ 0 ] );
		} else if ( el.value !== '' ) {
			data.append( el.name, el.value );
		}
	} );

	return data;
}

/**
 * Validator - validates from elements for required attribute and values
 *
 * @param form {HTMLFormElement}
 *
 */
class Validator {
	isRequired( input ) {
		return input.required || input.dataset.required || input.ariaRequired;
	}

	email( email ) {
		return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test( email );
	}

	password( password, options = {
		lower:     true,
		upper:     true,
		number:    true,
		minLength: 6,
		maxLength: false,
		special:   false
	} ) {
		const { lower, upper, number, minLength, maxLength, special } = options;

	}

	lower( value ) {
		return /^(?=.*[a-z])$/.test( value );
	}

	upper( value ) {
		return /^(?=.*[A-Z])$/.test( value );
	}

	length( value, min, max = false ) {
		const reg = new RegExp( `^(?=.*[a-zA-Z]).{${ min },${ max ? max : '' }$` );
		return reg.test( value );
	}

	symbols( value ) {
		return /^[●!"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~]$/.test( value );
	}

	digits( value ) {
		return /^(?=.*\d)$/.test( value );
	}

	checkValue( input ) {
		input.classList.remove( 'invalid' );

		if ( input.tagName === 'BUTTON' ) {
			return true;
		}

		if ( input.type === 'email' && !this.email( input.value ) ) {
			input.classList.add( 'invalid' );
			return false;
		}

		if ( !this.isRequired( input ) ) {
			return true;
		}

		if ( input.value === '' ) {
			input.classList.add( 'invalid' );
			return false;
		}

		return true;
	}

	checkValues( form ) {
		const invalid_elements = Array.from( form.elements ).filter( el => !this.checkValue( el ) );
		return invalid_elements.length === 0;
	}
}

/**
 * Empty all inputs in form
 * @param form
 * @constructor
 */
function EmptyFormInputs( form ) {
	Array.from( form.elements ).forEach( el => {
		if ( el.type != 'hidden' ) {
			el.value = '';
		}
		if ( el.type == 'file' ) {
			const label_txt = document.querySelector( `[for="${ el.id }"]` );
			if ( label_txt ) {
				label_txt.textContent = label_txt.dataset.text;
			}
		}
	} );
}

/**
 * Check file size for upload inputs
 * @param input
 * @param max_file_size
 * @returns {boolean}
 * @constructor
 */
function CheckFileSize( input, max_file_size ) {
	const file_size   = ( input.files[ 0 ].size / 1024 ).toFixed( 2 ),
	      max_size_kb = max_file_size * 1024;

	return file_size > max_size_kb;
}

/**
 * Function to get document current language code from html attribute lang
 * @returns {string} 'en', 'sr'
 * @constructor
 */
function GetDocumentLang() {
	return document.documentElement.lang.split( '-' )[ 0 ];
}

/**
 * Draw circle on canvas
 * @param context
 * @param width
 * @param x
 * @param y
 * @param color
 */
function DrawCircle( context, width, x, y, color = '#000' ) {
	context.beginPath();
	context.fillStyle = color;
	context.arc( x, y, width / 2, 0, 2 * Math.PI, false );
	context.fill();
}

/**
 * Draw text on canvas
 * @param context
 * @param txt
 * @param x
 * @param y
 * @param font
 * @param color
 */
function DrawText( context, txt, x, y, { font = '14px sans-serif', color = '#fff' } ) {
	context.font      = font;
	context.fillStyle = color;
	context.textAlign = 'center';
	context.fillText( txt, x, y );
}

/**
 * Emits custom event
 *
 * @param element {Document, Element, string} - default document
 * @param type
 * @param bubbles
 * @param cancelable
 * @constructor
 */
function Emmit( element, { type, bubbles = true, cancelable = false } ) {
	const event = document.createEvent( 'HTMLEvents' ),
	      el    = element === '' ? document : element instanceof Element ? element : document.querySelector( element );
	if ( !el ) {
		console.warn( 'Cannot find element', element );
		return;
	}
	event.initEvent( type, bubbles, cancelable );
	el.dispatchEvent( event );
}

/**
 *
 * Get all elements from nav including children as array of objects
 *
 * @param menu_items
 * @param arr
 * @returns {*}
 */
function GetLinks( menu_items = Element, arr = [] ) {
	Array.from( menu_items ).forEach( item => {
		let children_arr     = [];
		const item_link      = item.firstElementChild,
		      items_children = item.querySelector( '.sub-menu' ),
		      item_obj       = {
			      text: item_link.textContent,
			      link: item_link.href
		      };


		if ( items_children ) {
			item_obj.children = getLinks( items_children.children, children_arr );
		}

		arr.push( item_obj );
	} );

	return arr;
}

/**
 * Saves file to pc with provided data and file name
 *
 * @param data
 * @param filename
 *
 * @return Saves file to computer
 */
function Save( data, filename = 'console.json' ) {
	if ( !data ) {
		console.warn( 'Console.save: No data' );
		return;
	}

	if ( typeof data === 'object' ) {
		data = JSON.stringify( data, undefined, 4 );
	}
	let blob = new Blob( [ data ], { type: 'text/json' } ),
	    e    = document.createEvent( 'MouseEvents' ),
	    a    = document.createElement( 'a' );

	a.download            = filename;
	a.href                = window.URL.createObjectURL( blob );
	a.dataset.downloadurl = [ 'text/json', a.donwload, a.href ].join( ':' );
	e.initMouseEvent( 'click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null );
	a.dispatchEvent( e );
}

/**
 * Init download file from file url and it's name
 *
 * @param fileUrl
 * @param fileName
 */
function InitDownload( fileUrl, fileName = '' ) {
	if ( !fileUrl ) {
		console.warn( 'There is no URL provided for download!', fileUrl );
		return;
	}

	// It is necessary to create a new blob object with mime-type explicitly set
	// otherwise only Chrome works like it should
	const file = new Blob( [ fileUrl ], { type: 'application/pdf' } );

	// IE doesn't allow using a blob object directly as link href
	// instead it is necessary to use msSaveOrOpenBlob
	if ( window.navigator && window.navigator.msSaveOrOpenBlob ) {
		window.navigator.msSaveOrOpenBlob( file );
		return;
	}

	// For other browsers:
	// Create a link pointing to the ObjectURL containing the blob.
	const data = window.URL.createObjectURL( file ),
	      link = document.createElement( 'a' );

	link.href     = data;
	link.download = fileName || fileUrl.split( '/' ).pop();
	link.click();

	setTimeout( function () {
		// For Firefox it is necessary to delay revoking the ObjectURL
		window.URL.revokeObjectURL( data );
	}, 100 );
}

/**
 *
 * Controls anchor links or buttons in content for scrolling to element
 * Anchor needs to have anchor class and hash to the element
 * Button needs to have data-scroll attribute
 *
 * @param selector {string}
 * @param correction {number}
 * @constructor
 */
function AnchorScroll( selector, correction = 0 ) {
	if ( !selector ) {
		console.warn( 'You must provide a selector!' );
		return;
	}

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( selector ) ) return;

		const closest_el = e.target.closest( selector );
		let target_element_selector;

		if ( closest_el.tagName === 'A' ) {
			e.preventDefault();
			target_element_selector = closest_el.hash;
		} else {
			target_element_selector = closest_el.dataset.scroll;
		}


		const target_el = document.querySelector( target_element_selector );

		if ( !target_el ) {
			return;
		}

		setTimeout( () => {
			window.scroll( {
				               top:      target_el.offsetTop - correction,
				               behavior: 'smooth'
			               } );
		}, 50 );
	} );
}

/**
 * Add id's to headings for table of contents. Add event on click to scroll to clicked heading
 */
class TOC {
	/**
	 *
	 * @param toc_element
	 * @param headingsWrapper
	 * @param scrollOnClick
	 */
	constructor( { toc_element = '[data-toc]', headingsWrapper = '', scrollOnClick = true, correction = 0 } ) {
		this.toc_element = toc_element instanceof Element ? toc_element : document.querySelector( toc_element );
		if ( !this.toc_element ) return;

		this.links         = this.toc_element.querySelectorAll( 'a' );
		this.correction    = correction;
		this.scrollOnClick = scrollOnClick;

		// Headings to get from document
		this.headings = headingsWrapper ?
		                document.querySelectorAll( `${ headingsWrapper } ${ this.toc_element.dataset.toc }` ) :
		                document.querySelectorAll( this.toc_element.dataset.toc );

		if ( !this.headings ) {
			console.warn( 'There are no headings.' );
			return;
		}

		this.setId();

		if ( !this.links.length ) {
			this.createAnchors();
		}

		this.scrollOnClick && this.addScroll();
	}

	setId() {
		this.headings.forEach( ( heading, index ) => heading.id = `heading-${ index }` );
	}

	addScroll() {
		document.addEventListener( 'click', e => {
			if ( !e.target.closest( `.${ this.toc_element.classList[ 0 ] } a` ) ) return;

			e.preventDefault();

			const closest_el = e.target.closest( `.${ this.toc_element.classList[ 0 ] } a` ),
			      target_el  = document.querySelector( closest_el.hash );

			if ( !target_el ) {
				return;
			}

			setTimeout( () => {
				window.scroll( {
					               top:      target_el.offsetTop - this.correction,
					               behavior: 'smooth'
				               } );
			}, 50 );
		} );
	}

	createAnchors() {
		const anchorsFragment = document.createDocumentFragment();

		this.headings.forEach( heading => {
			const anchor       = document.createElement( 'a' );
			anchor.href        = `#${ heading.id }`;
			anchor.textContent = heading.textContent;
			anchorsFragment.appendChild( anchor );
		} );

		this.toc_element.appendChild( anchorsFragment );
	}
}

/**
 * Scroll to top
 * Controls showing button and handles click event to scroll to top
 *
 */
class ToTop {
	/**
	 * @param btn {string, Element} - HTML button element
	 * @param amount_to_scroll {number} - amount to scroll in pixels
	 */
	constructor( btn, amount_to_scroll = 800 ) {
		this.btn              = btn instanceof Element ? btn : document.querySelector( btn );
		this.amount_to_scroll = amount_to_scroll;

		if ( !this.btn ) {
			console.warn( `Cannot find or invalid selector for btn: '${ btn }'` );
			return;
		}

		this.showToTop();
		this.attachClick();
	}

	/**
	 * Checks if document is scrolled enough to show btn
	 */
	showToTop() {
		window.pageYOffset > this.amount_to_scroll ? this.btn.classList.add( 'show' ) : this.btn.classList.remove( 'show' );

		requestAnimationFrame( () => {
			this.showToTop( this.btn, this.amount_to_scroll );
		} );
	}

	/**
	 * Attaches click event on button
	 */
	attachClick() {
		this.btn.addEventListener( 'click', this.scrollToTop );
	}

	scrollToTop() {
		window.scrollTo( {
			                 top:      0,
			                 behavior: 'smooth'
		                 } );
	}
}

function addToCart( product_id ) {
	// Selection check
	if ( 'undefined' === typeof wc_add_to_cart_params ) {
		return false;
	}

	// Sending data for that specific product
	var data = {
		product_id: product_id,
		quantity:   1,
	};

	jQuery.post( wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'add_to_cart' ), data, function ( response ) {
		if ( !response ) {
			return;
		}

		// Redirect after addToCart
		if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
			window.location = wc_add_to_cart_params.cart_url;
			return;
		} else {
			return;
		}

		// Hashing and fragments for cart
		jQuery( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash ] );
	} );
}

export {
	RandomNumbers,
	ShowElement,
	HideElement,
	ToggleElement,
	Collapsible,
	ScrollItem,
	DrawCircle,
	DrawText,
	HeaderControl,
	addToCart,
	InitDownload,
	Delegate,
	Emmit,
	ToTop,
	HeightAnim,
	HeightAnim_2,
	RemoveAutoFill,
	CreateFormData,
	EmptyFormInputs,
	CheckFileSize,
	GetDocumentLang,
	AnchorScroll,
	TOC,
	Validator
};
