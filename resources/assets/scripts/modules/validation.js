const submit_btn = document.getElementById('submit');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');

function showError(input, message) {
    const formControl = input.parentElement;
    formControl.classList.add('error');
    const error_box = formControl.querySelector('error_box');
    error_box.innerText = message;
}

function showSuccess(input) {
    const formControl = input.parentElement;
    formControl.classList.add('success');
}

function checkEmail(input) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(input.value.trim())) {
        showSuccess(input)
    }else {
        showError(input,'Email is not invalid');
    }
}

function checkRequired(inputArr) {
    inputArr.forEach(function(input){
        if(input.value.trim() === ''){
            showError(input,`${getFieldName(input)} is required`)
        }else {
            showSuccess(input);
        }
    });
}

function checkLength(input, min ,max) {
    if(input.value.length < min) {
        showError(input, `${getFieldName(input)} must be at least ${min} characters`);
    }else if(input.value.length > max) {
        showError(input, `${getFieldName(input)} must be les than ${max} characters`);
    }else {
        showSuccess(input);
    }
}

function getFieldName(input) {
    return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}

submit_btn.addEventListener('submit',function(e) {
    e.preventDefault();

    checkRequired([username, email, password]);
    checkLength(username,3,15);
    checkLength(password,6,25);
    checkEmail(email);
});

/**
 * Class for setting and getting cookies from document
 */
export class Validation {
    /**
     *
     * @param form
     * @param submitButton
     * @param inputs
     * @param minLength
     * @param maxLength
     * @param errorBox
     * @param callback
     */
    constructor(form, submitButton, inputs = [], minLength = 3, maxLength = 25, errorBox, callback = false) {
        this.form = form instanceof Element ? form : document.querySelector(form);
        this.submitButton = submitButton instanceof Element ? submitButton : document.querySelector(submitButton);

        if (!this.form) {
            console.warn('Cannot find form with that ID: ', form);
            return;
        }

        this.submitButton = this.form.querySelector('#' + submitButton);

        this.inputs = inputs;
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.errorBox = errorBox;
        this.callback = callback;
    }

    showError(input, message) {
        const formControl = input.parentElement;
        formControl.classList.add('error');
        const error_box = formControl.querySelector('.' + error_box);
        error_box.innerText = message;
    }

    showSuccess(input) {
        const formControl = input.parentElement;
        formControl.classList.add('success');
    }

    checkEmail(input) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(input.value.trim())) {
            showSuccess(input)
        } else {
            showError(input, 'Email is not invalid');
        }
    }

    checkRequired(inputArr) {
        inputArr.forEach(function (input) {
            if (input.value.trim() === '') {
                showError(input, `${getFieldName(input)} is required`)
            } else {
                showSuccess(input);
            }
        });
    }

    checkLength(input, min, max) {
        if (input.value.length < min) {
            showError(input, `${getFieldName(input)} must be at least ${min} characters`);
        } else if (input.value.length > max) {
            showError(input, `${getFieldName(input)} must be les than ${max} characters`);
        } else {
            showSuccess(input);
        }
    }

    getFieldName(input) {
        return input.id.charAt(0).toUpperCase() + input.id.slice(1);
    }


    init() {

        if (!this.form) {
            return;
        }

        this.submitButton.addEventListener('click', () => {
            this.submitButton.preventDefault();

            this.checkRequired(inputs);
            this.checkLength(inputs[0], this.minLength, this.maxLength);
            this.checkLength(inputs[1], this.minLength, this.maxLength);
            this.checkEmail(inputs[2]);

            if (this.callback && this.callback instanceof Function) {
                this.callback();
            }
        });
    }
}
