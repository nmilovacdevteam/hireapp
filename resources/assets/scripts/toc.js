import './modules/global';

window.addEventListener('load', () => {
    document.addEventListener('click', function (e) {
        if (!e.target.closest('.terms_selector a')) return;

        e.preventDefault();

        document.querySelectorAll('.terms_selector a').forEach(function (el, index) {
            el.classList.remove('selected');
        })

        document.querySelector('.terms_switcher.show').classList.remove('show');

        e.target.classList.add('selected');
        document.getElementById(e.target.dataset.select).classList.add('show');
    })
});
