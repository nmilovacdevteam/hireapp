import './modules/global';
import Glide from '@glidejs/glide';
import CreateCustomSelect from './modules/custom-select';

window.addEventListener('load', () => {
    const slider_elements = document.querySelectorAll('.glide');

    new CreateCustomSelect('.wpcf7-select');

    slider_elements.length && slider_elements.forEach(slider => {
        if (slider.classList.contains('default__glide')) {
            const options = JSON.parse(slider.dataset.options);
            new Glide(`#${slider.id}`, options).mount();
        }
    });
});
