<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use App\Traits\Embed;
use App\Traits\Image;
use Roots\Sage\Config;
use Roots\Sage\Container;
use function App\template;

/**
 * Helper function for prettying up errors
 *
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ( $message, $subtitle = '', $title = '' ) {
	$title   = $title ?: __( 'Sage &rsaquo; Error', 'sage' );
	$footer  = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
	$message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
	wp_die( $message, $title );
};

/**
 * Ensure compatible version of PHP is used
 */
if ( version_compare( '7.1', phpversion(), '>=' ) ) {
	$sage_error( __( 'You must be using PHP 7.1 or greater.', 'sage' ), __( 'Invalid PHP version', 'sage' ) );
}

/**
 * Ensure compatible version of WordPress is used
 */
if ( version_compare( '5.2.5', get_bloginfo( 'version' ), '>=' ) ) {
	$sage_error( __( 'You must be using WordPress 5.2.6 or greater.', 'sage' ), __( 'Invalid WordPress version', 'sage' ) );
}

/**
 * Ensure dependencies are loaded
 */
if ( ! class_exists( 'Roots\\Sage\\Container' ) ) {
	if ( ! file_exists( $composer = __DIR__ . '/../vendor/autoload.php' ) ) {
		$sage_error(
			__( 'You must run <code>composer install</code> from the Sage directory.', 'sage' ),
			__( 'Autoloader not found.', 'sage' )
		);
	}
	require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map( function ( $file ) use ( $sage_error ) {
	$file = "../app/{$file}.php";
	if ( ! locate_template( $file, true, true ) ) {
		$sage_error( sprintf( __( 'Error locating <code>%s</code> for inclusion.', 'sage' ), $file ), 'File not found' );
	}
}, [
	'helpers',
	'setup',
	'filters',
	'admin',
	'custom-post-types',
	'NavWalker/megamenu-custom-fields',
	'NavWalker/Walker_nav'
] );

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
	'add_filter',
	[ 'theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri' ],
	array_fill( 0, 4, 'dirname' )
);
Container::getInstance()
         ->bindIf( 'config', function () {
	         return new Config( [
		         'assets' => require dirname( __DIR__ ) . '/config/assets.php',
		         'theme'  => require dirname( __DIR__ ) . '/config/theme.php',
		         'view'   => require dirname( __DIR__ ) . '/config/view.php',
	         ] );
         }, true );

/*
****
***** Custom code
****
 */

/**
 * Youtube embed
 * Shortcode call:
 * Without custom image: [youtube url="url" ratio="16:9 | 21:9 | 4:3 | 1:1"]
 * With custom image: [youtube url="url" ratio="16:9 | 21:9 | 4:3 | 1:1"] IMAGE [/youtube]
 *
 * @param      $atts
 * @param null $content
 *
 * @return string
 */
function youtube_embed( $atts, $content = null ): string {
	$atts = shortcode_atts( [
		'url'   => '#',
		'ratio' => '16:9'
	], $atts, 'youtube' );

	return Embed::youtubeVideo( [ 'url' => $atts['url'], 'ratio' => $atts['ratio'], 'image' => $content ] );
}

// Rest API
// Routes
add_action( 'rest_api_init', function () {
	register_rest_route( 'custom', '/load-category-posts', [
		'methods'             => 'GET',
		'callback'            => 'load_category_posts',
		'permission_callback' => '__return_true'
	] );
	register_rest_route( 'custom', '/load-posts', [
		'methods'             => 'GET',
		'callback'            => 'load_more_posts',
		'permission_callback' => '__return_true'
	] );
	register_rest_route( 'custom', '/load-faq', [
		'methods'             => 'GET',
		'callback'            => 'load_more_faqs',
		'permission_callback' => '__return_true'
	] );
	register_rest_route( 'custom', '/load-openings-posts', [
		'methods'             => 'GET',
		'callback'            => 'load_openings_posts',
		'permission_callback' => '__return_true'
	] );
} );


/**
 * Filter posts
 *
 * @param \WP_REST_Request $request
 *
 * @return \WP_REST_Response
 */
function load_more_posts( WP_REST_Request $request ) {
	$page       = $request->get_param( 'page' ) ?: 1;
	$taxonomy   = $request->get_param( 'taxonomy' );
	$term       = $request->get_param( 'term' );
	$post_type  = $request->get_param( 'post_type' );
	$offset     = $request->get_param( 'offset' );
	$posts_page = $request->get_param( 'posts_page' );
	$tax_query  = [];

	if ( $term != 0 ) {
		$tax_query = [
			[
				'taxonomy' => $taxonomy,
				'field'    => 'term_id',
				'terms'    => $term
			]
		];
	}

	if ( $posts_page ) {
		$posts = new WP_Query( [
			'post_type'      => $post_type,
			'posts_per_page' => get_option( 'posts_per_page' ) ?: 9,
			'offset'         => $page <= 1 ? $offset : '',
			'paged'          => $page
		] );
	} else {
		$posts = new WP_Query( [
			'post_type'      => $post_type,
			'posts_per_page' => get_option( 'posts_per_page' ) ?: 5,
			'tax_query'      => $tax_query,
			'offset'         => $page <= 1 ? $offset : '',
			'paged'          => $page
		] );
	}


	if ( ! $posts->post_count ) {
		return new WP_REST_Response( [
			'error' => __( 'There are no posts', THEME_TEXT_DOMAIN )
		], 404 );
	}

	$posts_html = template( 'partials.posts_rest', [ 'posts' => $posts->posts, 'is_rest' => true ] );

	return new WP_REST_Response( [
		'posts' => $posts_html
	], 200 );
}

/**
 * Filter Blogs
 *
 * @param \WP_REST_Request $request
 *
 * @return \WP_REST_Response
 */
function load_category_posts( WP_REST_Request $request ) {
	$term      = $request->get_param( 'term' );
	$tax_query = [];

	if ( $term != 0 ) {
		$tax_query = [
			[
				'taxonomy' => 'category',
				'field'    => 'term_id',
				'terms'    => $term
			]
		];
	}


	$posts = new WP_Query( [
		'post_type'      => 'post',
		'posts_per_page' => 9,
		'tax_query'      => $tax_query,
	] );


	if ( ! $posts->post_count ) {
		return new WP_REST_Response( [
			'error' => __( 'There are no posts', THEME_TEXT_DOMAIN )
		], 404 );
	}

	$posts_html = template( 'partials.posts_rest', [ 'posts' => $posts->posts, 'is_rest' => true ] );

	return new WP_REST_Response( [
		'posts' => $posts_html
	], 200 );
}

function load_openings_posts( WP_REST_Request $request ) {
	$term      = $request->get_param( 'term' );
	$tax_query = [];

	if ( $term != 0 ) {
		$tax_query = [
			[
				'taxonomy' => 'opening',
				'field'    => 'term_id',
				'terms'    => $term
			]
		];
	}


	$openings = new WP_Query( [
		'post_type'      => 'careers',
		'posts_per_page' => 9,
		'tax_query'      => $tax_query,
	] );


	if ( ! $openings->post_count ) {
		return new WP_REST_Response( [
			'error' => __( 'There are no posts', THEME_TEXT_DOMAIN )
		], 404 );
	}

	$openings_html = template( 'partials.openings', [ 'openings' => $openings->posts, 'is_rest' => true ] );

	return new WP_REST_Response( [
		'openings' => $openings_html
	], 200 );
}

/**
 * Filter posts
 *
 * @param \WP_REST_Request $request
 *
 * @return \WP_REST_Response
 */
function load_more_faqs( WP_REST_Request $request ) {
	$search = $request->get_param( 'search' ) ?: 1;


	if ( ! $search ) {
		return;
	}


	$faqs = new WP_Query( [
		'post_type'      => 'faq',
		'posts_per_page' => -1,
		's'=>$search
	] );


	if ( ! $faqs->post_count ) {
		return new WP_REST_Response( [
			'error' => __( 'There are no posts', THEME_TEXT_DOMAIN )
		], 404 );
	}

	$faqs_html = template( 'partials.faqs_rest', [ 'faqs' => $faqs->posts, 'is_rest' => true ] );

	return new WP_REST_Response( [
		'faqs' => $faqs_html
	], 200 );
}
