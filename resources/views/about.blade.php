{{--
Template Name: About Us
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    <div class="background-effect">
        <img src="@asset('images/bg.png')" alt="">
    </div>
    <section class="first_fold">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mx-auto">
                    @if($fields['title'])
                        <h1 class="global_title">{!! apply_filters('the_content', $fields['title']) !!}</h1>
                    @else {
                    <h1 class="global_title">{!! \App\Controllers\App::title() !!}</h1>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="content_after_first_fold">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    {!! \App\Traits\Image::getAcfImage(['acf_field'=>$fields['fold_image'],'image_sizes'=>['about_fold']]) !!}
                </div>
                <div class="col-lg-6 content_bottom">
                    <div class="content">
                        {!! apply_filters('the_content', the_content()) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-team">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    @if($fields['team_title'])
                        <h2 class="global_title">{!! $fields['team_title'] !!}</h2>
                    @endif
                </div>
                @if($fields['members'])
                    <div class="col-lg-12 authors_wrapper">
                        <svg xmlns="http://www.w3.org/2000/svg" width="809" height="696" viewBox="0 0 809 696"
                             fill="none">
                            <g clip-path="url(#clip0_107_4699)">
                                <path opacity="0.15" fill-rule="evenodd" clip-rule="evenodd"
                                      d="M291.968 456.28C432.579 518.713 632.776 365.054 561.373 306.904C446.785 213.798 151.93 393.955 291.968 456.28Z"
                                      stroke="#1089FF" stroke-miterlimit="10"/>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M646.664 345.134C661.84 290.421 643.639 236.473 576.973 209.513C405.086 139.93 212.002 309.199 160.979 421.216C153.012 438.107 146.899 457.727 157.474 476.965C164.806 491.428 180.346 503.298 196.869 510.011C287.639 547.504 405.719 532.003 500.327 487.087C577.754 451.366 633.044 394.798 646.664 345.134Z"
                                      stroke="#3C90EE" stroke-opacity="0.12" stroke-miterlimit="10"
                                      stroke-linecap="round"
                                      stroke-linejoin="round"/>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M485.038 554.843C585.998 517.075 676.93 457.205 731.951 383.364C796.356 297.652 788.366 196.359 693.294 137.854C458.544 -6.66098 200.403 239.287 59.5673 408.418C38.0724 434.015 16.6305 465.56 22.2939 498.114C26.5141 522.673 45.8973 542.402 67.9549 554.327C184.079 614.469 344.642 606.472 485.038 554.843Z"
                                      stroke="#3C90EE" stroke-opacity="0.08" stroke-miterlimit="10"/>
                            </g>
                            <defs>
                                <clipPath id="clip0_107_4699">
                                    <rect width="713.512" height="572.28" fill="white"
                                          transform="translate(107.153) rotate(10.7917)"/>
                                </clipPath>
                            </defs>
                        </svg>

                        @foreach($fields['members'] as $member)
                            <div class="authors">
                                {!! \App\Traits\Image::getAcfImage(['acf_field'=>$member['member_image'],'image_sizes'=>['members']]) !!}
                                @if($member['member_name'])
                                    <p class="author_name">{!! $member['member_name'] !!}</p>
                                @endif
                                @if($member['member_role'])
                                    <p class="author_role">{!! $member['member_role'] !!}</p>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </section>
    @endwhile
@endsection
