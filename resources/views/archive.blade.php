@extends('layouts.app')

@section('content')
    <div class="background-effect">
        <img src="@asset('images/bg.png')" alt="">
    </div>
    <section class="first_fold">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @if($title)
                        <h1 class="global_title">{!! $title !!}</h1>
                    @else
                        <h1 class="global_title"><span>{!! \App\Controllers\App::title() !!}</span></h1>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @if($featured)
        <section class="featured">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-1">
                        {!! \App\Traits\Image::getImage(['post_id'=>$featured[0]->ID,'image_sizes'=>['blog']]) !!}
                    </div>
                    <div class="col-lg-5">
                        @if(get_terms('category', $featured[0]->ID))
                            <div class="category">
                                <a href="{{ get_category_link(get_terms('category', $featured[0]->ID)[0]->term_id) }}">{{ get_terms('category', $featured[0]->ID)[0]->name }}</a>
                            </div>
                        @endif
                        <h3>{{ $featured[0]->post_title }}</h3>
                        <div class="post_info">
                            <span>{{ __('by',THEME_TEXT_DOMAIN) }} {{ get_the_author_meta( 'first_name', $featured[0]->post_author  ) }} {{ get_the_author_meta( 'last_name', $featured[0]->post_author  ) }}</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="2" height="2" viewBox="0 0 2 2" fill="none">
                                <circle cx="1" cy="1" r="1" fill="#FFA41B"/>
                            </svg>
                            <span>{{ get_the_date('M d, Y',$featured[0]->ID) }}</span>
                        </div>
                        <div class="content">
                            <p> {!! \App\shorten_text(get_the_excerpt($featured[0]->ID), 200) !!}</p>
                        </div>
                        <a class="read_more"
                           href="{{ get_permalink($featured[0]->ID) }}">{{ __('Read more',THEME_TEXT_DOMAIN) }}</a>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @include('partials.newsletter')
    <section class="posts">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="category_picker">
                        <a data-category="0" {{ is_home() ? 'class=selected': '' }}>
                            {{ __('All Categories', THEME_TEXT_DOMAIN) }}
                        </a>
                        @if(isset($category_links))
                            @foreach($category_links as $category)
                                <a {{ $category->term_id == get_queried_object_id() ? 'class=selected' : '' }} data-category="{{ $category->term_id }}">
                                    {{ $category->name }}
                                </a>
                            @endforeach
                        @endif
                    </div>
                    <div class="category_picker_mobile">
                        <select name="picker" id="picker" class="category_picker_select">
                            <option value="0" {{ is_home() ? 'selected': '' }}>
                                {{ __('All Categories', THEME_TEXT_DOMAIN) }}
                            </option>
                            @if(isset($category_links))
                                @foreach($category_links as $category)
                                    <option {{ $category->term_id == get_queried_object_id() ? 'class=selected' : '' }} value="{{ $category->term_id }}">
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row posts_output">
                @php $args = [
	                'posts_per_page'=>9,
	                'tax_query'=> [
	                	'taxonomy'=>'category',
                        'terms'=>get_queried_object_id(),
                        'field'=>'term_id']
                   ]; @endphp

                @php $query = new WP_Query( $args ); @endphp
                @if ( $query->have_posts() )
                    @while ( $query->have_posts() )
                        @php $query->the_post(); @endphp
                        @include('partials.posts')
                    @endwhile
                @endif


                @php wp_reset_postdata(); @endphp
            </div>


            @if($query->max_num_pages > 1)
                <div class="row">
                    <div class="col-lg-4 mx-auto load_more_wrapper">
                        <a data-page="2"
                           data-max_pages="{{ $query->max_num_pages }}" data-offset="9" data-tax="category"
                           data-term="{{ get_queried_object_id() }}" data-posts_page="{{ is_home() ? 'true' : '' }}"
                           data-post_type="post"
                           class="load_more btn--load">{{ __('Load more', THEME_TEXT_DOMAIN) }}</a>
                    </div>
                </div>
            @endif
        </div>
    </section>

@endsection
