{{--
Template Name: Careers
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    <div class="background-effect">
        <img src="@asset('images/bg.png')" alt="">
    </div>
    <section class="first_fold">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mx-auto">
                    <h1 class="global_title">
                        {!! $fields['title'] !!}
                    </h1>

                    @if($fields['content'])
                        <div class="content">
                            <p>{!! $fields['content'] !!}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="image">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    {!! \App\Traits\Image::getAcfImage(['acf_field'=>$fields['fold_image'],'image_sizes'=>['team_featured']]) !!}
                </div>
            </div>
            @if($fields['subtext'])
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <div class="content">
                            <p>{!! $fields['subtext'] !!}</p>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <section class="openings">
        <div class="container">
            @if($fields['openings_title'])
                <div class="row">
                    <div class="col-lg-4 mx-auto">
                        <h2>{{ $fields['openings_title'] }}</h2>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="openings_picker">
                    <a class="selected" data-opening="0">{{ __('All position', THEME_TEXT_DOMAIN) }}</a>
                    @if(isset($openings_links))
                        @foreach($openings_links as $openings)
                            <a data-opening="{{ $openings->term_id }}">
                                {{ $openings->name }}
                            </a>
                        @endforeach
                    @endif
                </div>
                <div class="openings_picker_mobile">
                    <select name="picker" id="picker" class="openings_picker_select">
                        <option value="0" {{ is_home() ? 'selected': '' }}>
                            {{ __('All positions', THEME_TEXT_DOMAIN) }}
                        </option>
                        @if(isset($openings_links))
                            @foreach($openings_links as $openings)
                                <option value="{{ $category->term_id }}">
                                    {{ $openings->name }}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="listing_openings">
                    @php $args = [
	'post_type'=>'careers',
	'posts_per_page'=>3
]; @endphp

                    @php $query = new WP_Query( $args ); @endphp
                    @if ( $query->have_posts() )
                        @while ( $query->have_posts() )
                            @php $query->the_post(); @endphp
                            <div class="col-lg-12">
                                <div class="single_listing row">
                                    <div class="col-lg-12">
                                        <div class="category">
                                            <a>{{ get_the_terms(get_the_ID(),'opening')[0]->name }}</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h3>{{ get_the_title() }}</h3>
                                    </div>
                                    <div class="location col-lg-12">{{ get_field('location') }}</div>
                                    <div class="content col-lg-9">
                                        <p>{!! \App\shorten_text(get_the_content()) !!}</p>
                                    </div>
                                    <div class="col-lg-3 apply_wrapper">
                                        <div class="apply">
                                            <a data-title="{{ get_the_title() }}" data-open="#apply">{{ __('Apply', THEME_TEXT_DOMAIN) }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endwhile
                    @endif
                    @php wp_reset_postdata(); @endphp
                </div>
            </div>
        </div>
    </section>
    @component('components.modal',['modal_id'=>'apply','modal_header'=>false])
        <div class="row">
            <div class="modal_wrapper col-lg-12">
                <div class="row">
                    <div class="col-lg-3 mx-auto svg_center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="38" height="48" viewBox="0 0 38 48" fill="none">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M14.25 0C10.315 0 7.125 3.18997 7.125 7.125V7.12662C4.56273 7.13779 3.15765 7.2262 2.11104 7.92552C1.59239 8.27207 1.14707 8.71739 0.800519 9.23604C0 10.4341 0 12.1019 0 15.4375V38C0 42.4783 0 44.7175 1.39124 46.1088C2.78249 47.5 5.02166 47.5 9.5 47.5H28.5C32.9783 47.5 35.2175 47.5 36.6088 46.1088C38 44.7175 38 42.4783 38 38V15.4375C38 12.1019 38 10.4341 37.1995 9.23604C36.8529 8.71739 36.4076 8.27207 35.889 7.92552C34.8424 7.2262 33.4373 7.13779 30.875 7.12662V7.125C30.875 3.18997 27.685 0 23.75 0H14.25ZM14.25 7.125C14.25 5.81332 15.3133 4.75 16.625 4.75H21.375C22.6867 4.75 23.75 5.81332 23.75 7.125C23.75 8.43668 22.6867 9.5 21.375 9.5H16.625C15.3133 9.5 14.25 8.43668 14.25 7.125ZM11.875 21.375C10.5633 21.375 9.5 22.4383 9.5 23.75C9.5 25.0617 10.5633 26.125 11.875 26.125H26.125C27.4367 26.125 28.5 25.0617 28.5 23.75C28.5 22.4383 27.4367 21.375 26.125 21.375H11.875ZM11.875 30.875C10.5633 30.875 9.5 31.9383 9.5 33.25C9.5 34.5617 10.5633 35.625 11.875 35.625H21.375C22.6867 35.625 23.75 34.5617 23.75 33.25C23.75 31.9383 22.6867 30.875 21.375 30.875H11.875Z"
                                  fill="#1089FF"/>
                        </svg>
                    </div>

                    <div class="col-lg-12 mx-auto">
                        <h3>Submit an application</h3>
                    </div>
                    <div class="col-lg-12">
                        {!! do_shortcode('[contact-form-7 id="39" title="Apply"]') !!}
                    </div>
                </div>
            </div>
        </div>
    @endcomponent
    @endwhile
@endsection
