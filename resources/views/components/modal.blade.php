<?php
if ( !isset( $slot ) ) {
	return;
}
$modal_id          = $modal_id ?? 'modal-' . time();
$modal_classes     = $modal_classes ?? '';
$close_btn_classes = $close_btn_classes ?? '';
$modal_header      = $modal_header ?? true;
?>
<div class="modal {{ !is_null($modal_animation) ? "modal--$modal_animation" : '' }} {{ $modal_classes }}" id="{{ $modal_id }}">
	<div class="modal__content container">
		@if($modal_header)
			<div class="modal__header">
				{!! \App\Controllers\App::closeBtn( '#' . $modal_id , $close_btn_classes ) !!}
			</div>
		@endif
		<div class="modal__body">
			{!! $slot !!}
		</div>
	</div>
</div>
