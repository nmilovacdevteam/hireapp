{{--
Template Name: FAQ
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    <section class="first_fold">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="global_title">{!! $fields['title'] !!}</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="search">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @if($fields['subtext'])
                        <div class="subtext">{!! $fields['subtext'] !!}</div>
                    @endif
                    <div class="search_wrapper">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <circle cx="11" cy="11" r="7" stroke="#BFD4ED" stroke-width="2"/>
                            <path d="M11 8C10.606 8 10.2159 8.0776 9.85195 8.22836C9.48797 8.37913 9.15726 8.6001 8.87868 8.87868C8.6001 9.15726 8.37913 9.48797 8.22836 9.85195C8.0776 10.2159 8 10.606 8 11"
                                  stroke="#BFD4ED" stroke-width="2" stroke-linecap="round"/>
                            <path d="M20 20L17 17" stroke="#BFD4ED" stroke-width="2" stroke-linecap="round"/>
                        </svg>
                        <input type="text" id="search" name="search" placeholder="Search for question">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="selection">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 single_selection">
                    <div data-target="workers" class="single_selection-wrapper active">
                        <svg xmlns="http://www.w3.org/2000/svg" width="54" height="56" viewBox="0 0 54 56" fill="none">
                            <g filter="url(#filter0_d_107_4026)">
                                <path d="M45.1109 44.0589C44.0426 40.958 41.6886 38.2179 38.4141 36.2636C35.1395 34.3093 31.1273 33.25 26.9998 33.25C22.8723 33.25 18.8601 34.3093 15.5855 36.2636C12.3109 38.2179 9.95695 40.958 8.88867 44.0589L26.9998 47.8333L45.1109 44.0589Z"
                                      fill="url(#paint0_linear_107_4026)"/>
                            </g>
                            <g filter="url(#filter1_d_107_4026)">
                                <ellipse cx="26.9997" cy="18.6667" rx="10.4167" ry="10.4167"
                                         fill="url(#paint1_linear_107_4026)"/>
                            </g>
                            <defs>
                                <filter id="filter0_d_107_4026" x="0.888672" y="25.25" width="52.2227" height="30.5833"
                                        filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                                   values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                                   result="hardAlpha"/>
                                    <feOffset/>
                                    <feGaussianBlur stdDeviation="4"/>
                                    <feComposite in2="hardAlpha" operator="out"/>
                                    <feColorMatrix type="matrix"
                                                   values="0 0 0 0 0.0871875 0 0 0 0 0.205347 0 0 0 0 0.3375 0 0 0 0.19 0"/>
                                    <feBlend mode="multiply" in2="BackgroundImageFix"
                                             result="effect1_dropShadow_107_4026"/>
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_107_4026"
                                             result="shape"/>
                                </filter>
                                <filter id="filter1_d_107_4026" x="8.58301" y="0.25" width="36.833" height="36.8333"
                                        filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                                   values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                                   result="hardAlpha"/>
                                    <feOffset/>
                                    <feGaussianBlur stdDeviation="4"/>
                                    <feComposite in2="hardAlpha" operator="out"/>
                                    <feColorMatrix type="matrix"
                                                   values="0 0 0 0 0.0871875 0 0 0 0 0.205347 0 0 0 0 0.3375 0 0 0 0.19 0"/>
                                    <feBlend mode="multiply" in2="BackgroundImageFix"
                                             result="effect1_dropShadow_107_4026"/>
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_107_4026"
                                             result="shape"/>
                                </filter>
                                <linearGradient id="paint0_linear_107_4026" x1="43.5411" y1="32.5699" x2="26.8417"
                                                y2="130.914" gradientUnits="userSpaceOnUse">
                                    <stop offset="0.1295" stop-color="#E9F5FF"/>
                                    <stop offset="0.3528" stop-color="#B2BFCA"/>
                                    <stop offset="0.8631" stop-color="#283846"/>
                                    <stop offset="1" stop-color="#021321"/>
                                </linearGradient>
                                <linearGradient id="paint1_linear_107_4026" x1="36.5135" y1="7.27844" x2="-15.2427"
                                                y2="129.991" gradientUnits="userSpaceOnUse">
                                    <stop offset="0.1295" stop-color="#E9F5FF"/>
                                    <stop offset="0.3528" stop-color="#B2BFCA"/>
                                    <stop offset="0.8631" stop-color="#283846"/>
                                    <stop offset="1" stop-color="#021321"/>
                                </linearGradient>
                            </defs>
                        </svg>
                        <div class="single_selection-content">
                            <p class="title">FAQ for Workers</p>
                            <p class="content">Most frequent asked questions by workers</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 single_selection">
                    <div data-target="business" class="single_selection-wrapper">
                        <svg xmlns="http://www.w3.org/2000/svg" width="55" height="61" viewBox="0 0 55 61" fill="none">
                            <g clip-path="url(#clip0_107_3989)">
                                <g filter="url(#filter0_d_107_3989)">
                                    <path d="M27.46 34.7564L32.9466 49.71L32.9853 49.8251L34.724 54.5029L37.506 48.5214L43.4176 51.1287L41.7948 46.6426L41.7562 46.5276L36.2695 31.574L27.46 34.7564Z"
                                          fill="url(#paint0_linear_107_3989)"/>
                                    <path d="M26.9577 34.7564L21.471 49.71L21.4324 49.8251L19.7323 54.5029L16.9117 48.5214L11 51.1287L12.6228 46.6426L12.6615 46.5276L18.1481 31.574L26.9577 34.7564Z"
                                          fill="url(#paint1_linear_107_3989)"/>
                                    <path d="M41.3697 18.7291L41.2538 15.7384L39.2059 13.5529L37.9308 10.8305L35.1875 9.56524L33.0238 7.57142L30.01 7.45639L27.228 6.42114L24.4074 7.45639L21.3936 7.57142L19.1912 9.60358L16.4479 10.8689L15.1728 13.5912L13.125 15.7767L13.0091 18.7675L11.9658 21.5665L13.0091 24.3655L13.125 27.3562L15.1728 29.5417L16.4479 32.2641L19.1912 33.5294L21.3936 35.5615L24.4074 35.6766L27.228 36.7118L30.0486 35.6766L33.0624 35.5615L35.2648 33.5294L38.0081 32.2641L39.2832 29.5417L41.331 27.3562L41.4469 24.3655L42.4902 21.5665L41.3697 18.7291ZM27.228 33.491C20.5822 33.491 15.1728 28.1231 15.1728 21.5281C15.1728 14.9332 20.5822 9.56524 27.228 9.56524C33.8738 9.56524 39.2832 14.9332 39.2832 21.5281C39.2832 28.1231 33.8738 33.491 27.228 33.491Z"
                                          fill="url(#paint2_linear_107_3989)"/>
                                    <path d="M27.2282 10.9072C21.3166 10.9072 16.5254 15.6617 16.5254 21.5281C16.5254 27.3946 21.3166 32.1107 27.2282 32.1107C33.1399 32.1107 37.9311 27.3562 37.9311 21.4898C37.9311 15.6234 33.1399 10.9072 27.2282 10.9072ZM31.1307 27.0878L27.151 25.0173L23.1712 27.0878L23.944 22.6784L20.6983 19.5727L25.1418 18.9208L27.151 14.8949L29.1602 18.9208L33.6036 19.5727L30.358 22.6784L31.1307 27.0878Z"
                                          fill="url(#paint3_linear_107_3989)"/>
                                </g>
                            </g>
                            <defs>
                                <filter id="filter0_d_107_3989" x="3" y="-1.57886" width="48.418" height="64.0818"
                                        filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                    <feColorMatrix in="SourceAlpha" type="matrix"
                                                   values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                                   result="hardAlpha"/>
                                    <feOffset/>
                                    <feGaussianBlur stdDeviation="4"/>
                                    <feComposite in2="hardAlpha" operator="out"/>
                                    <feColorMatrix type="matrix"
                                                   values="0 0 0 0 0.0871875 0 0 0 0 0.205347 0 0 0 0 0.3375 0 0 0 0.19 0"/>
                                    <feBlend mode="multiply" in2="BackgroundImageFix"
                                             result="effect1_dropShadow_107_3989"/>
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_107_3989"
                                             result="shape"/>
                                </filter>
                                <linearGradient id="paint0_linear_107_3989" x1="42.7261" y1="30.5047" x2="-27.7844"
                                                y2="146.855" gradientUnits="userSpaceOnUse">
                                    <stop offset="0.1295" stop-color="#E9F5FF"/>
                                    <stop offset="0.3528" stop-color="#B2BFCA"/>
                                    <stop offset="0.8631" stop-color="#283846"/>
                                    <stop offset="1" stop-color="#021321"/>
                                </linearGradient>
                                <linearGradient id="paint1_linear_107_3989" x1="26.2661" y1="30.5047" x2="-44.2443"
                                                y2="146.855" gradientUnits="userSpaceOnUse">
                                    <stop offset="0.1295" stop-color="#E9F5FF"/>
                                    <stop offset="0.3528" stop-color="#B2BFCA"/>
                                    <stop offset="0.8631" stop-color="#283846"/>
                                    <stop offset="1" stop-color="#021321"/>
                                </linearGradient>
                                <linearGradient id="paint2_linear_107_3989" x1="41.1673" y1="5.00854" x2="-33.6801"
                                                y2="183.839" gradientUnits="userSpaceOnUse">
                                    <stop offset="0.1295" stop-color="#E9F5FF"/>
                                    <stop offset="0.3528" stop-color="#B2BFCA"/>
                                    <stop offset="0.8631" stop-color="#283846"/>
                                    <stop offset="1" stop-color="#021321"/>
                                </linearGradient>
                                <linearGradient id="paint3_linear_107_3989" x1="37.0034" y1="9.9184" x2="-15.3233"
                                                y2="135.167" gradientUnits="userSpaceOnUse">
                                    <stop offset="0.1295" stop-color="#E9F5FF"/>
                                    <stop offset="0.3528" stop-color="#B2BFCA"/>
                                    <stop offset="0.8631" stop-color="#283846"/>
                                    <stop offset="1" stop-color="#021321"/>
                                </linearGradient>
                                <clipPath id="clip0_107_3989">
                                    <rect width="55" height="61" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                        <div class="single_selection-content">
                            <p class="title">FAQ for Business</p>
                            <p class="content">Most frequent asked questions by businesses</p>
                        </div>
                    </div>
                </div>
            </div>
            @if($faqs->post_count > 0)
                <div class="row">
                    <div class="col-lg-12">
                        <div id="accordion">
                            @foreach($faqs->posts as $faq)
                                <div class="{{ strtolower(get_the_terms($faq->ID,'type')[0]->name) }} {{ strtolower(get_the_terms($faq->ID,'type')[0]->name) == 'workers' ? 'showed' : '' }} collapsible">
                                    <button class="btn {{ $loop->index == 0 ? 'active': '' }}" data-collapse="#collapse-{{ $loop->index }}" data-parent="#accordion">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"
                                             fill="none">
                                            <path d="M5.76953 4.80794L1.31215 1.09346C0.791086 0.659238 0 1.02976 0 1.70803V8.29197C0 8.97024 0.791085 9.34076 1.31215 8.90654L5.76953 5.19206C5.88947 5.09211 5.88947 4.90789 5.76953 4.80794Z"
                                                  fill="#FFA41B"/>
                                        </svg>
                                        <span>{{ $faq->post_title }}</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none">
                                            <path d="M12 6L12 18" stroke="#FFA41B" stroke-width="2"
                                                  stroke-linecap="round"/>
                                            <path d="M18 12L6 12" stroke="#FFA41B" stroke-width="2"
                                                  stroke-linecap="round"/>
                                        </svg>
                                    </button>
                                    <div id="collapse-{{ $loop->index }}" class="collapse {{ $loop->index == 0 ? 'show' : '' }}">
                                        {!! apply_filters('the_content', $faq->post_content) !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
    @endwhile
@endsection
