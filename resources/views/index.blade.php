@extends('layouts.app')

@section('content')
	@include('partials.page-header')

	@if (!have_posts())
		<div class="alert alert-warning">
			{{ __('Nema rezultata.', THEME_TEXT_DOMAIN) }}
		</div>
		{!! get_search_form(false) !!}
	@endif

	@while (have_posts()) @php the_post() @endphp
	@include('partials.content-'.get_post_type())
	@endwhile

	{!! get_the_posts_navigation() !!}
@endsection
