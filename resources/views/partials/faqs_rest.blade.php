@foreach($faqs as $faq)
    <div class="{{ strtolower(get_the_terms($faq->ID,'type')[0]->name) }} {{ strtolower(get_the_terms($faq->ID,'type')[0]->name) == 'workers' ? 'showed' : '' }} collapsible">
        <button class="btn {{ $loop->index == 0 ? 'active': '' }}" data-collapse="#collapse-{{ $loop->index }}"
                data-parent="#accordion">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"
                 fill="none">
                <path d="M5.76953 4.80794L1.31215 1.09346C0.791086 0.659238 0 1.02976 0 1.70803V8.29197C0 8.97024 0.791085 9.34076 1.31215 8.90654L5.76953 5.19206C5.88947 5.09211 5.88947 4.90789 5.76953 4.80794Z"
                      fill="#FFA41B"/>
            </svg>
            <span>{{ $faq->post_title }}</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                 viewBox="0 0 24 24"
                 fill="none">
                <path d="M12 6L12 18" stroke="#FFA41B" stroke-width="2"
                      stroke-linecap="round"/>
                <path d="M18 12L6 12" stroke="#FFA41B" stroke-width="2"
                      stroke-linecap="round"/>
            </svg>
        </button>
        <div id="collapse-{{ $loop->index }}" class="collapse {{ $loop->index == 0 ? 'show' : '' }}">
            {!! apply_filters('the_content', $faq->post_content) !!}
        </div>
    </div>
@endforeach
