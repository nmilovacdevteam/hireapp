@if($footer_form)
    <div class="contact_prefooter" id="prefooter">
        <div class="container">
            <div class="row">
                <div class="contact_wrapper">
                    {!! do_shortcode($footer_form) !!}
                </div>
            </div>
        </div>
    </div>
@endif
<footer id="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo_footer">
                    <img src="@asset('images/logo_footer.png')" alt="">
                    <div class="mobile_nav">
                        @if (has_nav_menu('primary_navigation'))
                            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'useful-links footer-menu']) !!}
                        @endif
                    </div>
                    {!! $primary_navigation ? $primary_navigation->nav : '' !!}
                </div>
                <div class="social_wrapper">
                    @include('partials.socials', ['socials' => $socials])
                    <div class="mobile_nav">
                        @if (has_nav_menu('secondary_navigation'))
                            {!! wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'useful-links footer-menu']) !!}
                        @endif
                    </div>
                    {!! $secondary_navigation ? $secondary_navigation->nav : '' !!}
                </div>
                <div class="copyright">
                    <span>{{ __('© Copyright 2021 By HireApp Technologies, inc. | All rights reserved', THEME_TEXT_DOMAIN) }}</span>
                    <div class="toc">
                        <div class="mobile_nav">
                            @if (has_nav_menu('third_navigation'))
                                {!! wp_nav_menu(['theme_location' => 'third_navigation', 'menu_class' => 'useful-links footer-menu']) !!}
                            @endif
                        </div>
                        {!! $third_navigation ? $third_navigation->nav : '' !!}
                    </div>
                    <div class="dev">
                        <a href="https://b22.io">{{ __('Design & dev by b22',THEME_TEXT_DOMAIN) }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
{!! $cookie !!}
