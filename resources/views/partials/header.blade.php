<header id="header">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-lg-1"></div>
            <div class="col-lg-10">
                <nav class="navbar primary_navigation">
                    {!! $logo !!}

                    {!! $primary_navigation ? $primary_navigation->toggler : '' !!}

                    {!! $primary_navigation ? $primary_navigation->nav : '' !!}

                    <a href="#prefooter" class="nav__link join_us">{{ __('Join Us', THEME_TEXT_DOMAIN) }}</a>
                </nav>
            </div>
        </div>
    </div>
</header>
