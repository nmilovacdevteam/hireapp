<section class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="newsletter_wrapper">
                    <img src="@asset('images/mailbox.png')" alt="">
                    <img src="@asset('images/red-newsletter.png')" alt="">
                    <div class="title">
                        {{ $newsletter->title }}
                    </div>
                    <div class="content">
                        <p>{!! $newsletter->content !!}</p>
                    </div>
                    <div class="form_newsletter">
                        <input type="email" placeholder="Enter your email">
                        <button>Subscribe</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
