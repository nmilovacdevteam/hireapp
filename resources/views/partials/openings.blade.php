@foreach($openings as $openings)
    <div class="col-lg-12">
        <div class="single_listing row">
            <div class="col-lg-12">
                <div class="category">
                    <a>{{ get_the_terms($openings->ID,'opening')[0]->name }}</a>
                </div>
            </div>
            <div class="col-lg-12">
                <h3>{{ get_the_title($openings->ID) }}</h3>
            </div>
            <div class="location col-lg-12">{{ get_field('location',$openings->ID) }}</div>
            <div class="content col-lg-9">
                <p>{!! \App\shorten_text($openings->post_content) !!}</p>
            </div>
            <div class="col-lg-3 apply_wrapper">
                <div class="apply">
                    <a data-title="{{ get_the_title($openings->ID) }}" data-open="#apply">{{ __('Apply', THEME_TEXT_DOMAIN) }}</a>
                </div>
            </div>
        </div>
    </div>
@endforeach
