<div class="col-lg-4">
    <div class="posts_wrapper">
        <div class="image">
            @if(get_terms('category', get_the_ID()))
                <div class="category">
                    <a href="{{ get_category_link(get_terms('category', get_the_ID())[0]->term_id) }}">{{ get_terms('category', get_the_ID())[0]->name }}</a>
                </div>
            @endif
            {!! \App\Traits\Image::getImage(['post_id'=>get_the_ID(),'image_sizes'=>['blog']]) !!}
        </div>
        <h3><a href="{{ get_permalink(get_the_ID()) }}">{{ get_the_title() }}</a></h3>
        @php $author_id = get_post_field( 'post_author', get_the_ID() );@endphp
        <div class="post_info">
            <span>{{ __('by',THEME_TEXT_DOMAIN) }} {{ get_the_author_meta( 'first_name', $author_id  ) }} {{ get_the_author_meta( 'last_name', $author_id  ) }}</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="2" height="2" viewBox="0 0 2 2" fill="none">
                <circle cx="1" cy="1" r="1" fill="#FFA41B"/>
            </svg>
            <span>{{ get_the_date('M d, Y',get_the_ID()) }}</span>
        </div>
        <div class="content">
            <p>{!! \App\shorten_text(get_the_excerpt(get_the_ID()), 200) !!}</p>
        </div>
        <a class="read_more"
           href="{{ get_permalink(get_the_ID()) }}">{{ __('Read more',THEME_TEXT_DOMAIN) }}</a>
    </div>
</div>
