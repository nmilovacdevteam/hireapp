@extends('layouts.app')

@section('content')
	<?php
	$searched_item = get_search_query();

	$search_query = new WP_Query( [
		'post_type'      => [ 'any' ],
		's'              => $searched_item,
		'sentence'       => true,
		'posts_per_page' => -1
	] );
	?>
	<div class="container">
		<div class="row">
			@if (!$search_query->have_posts())
				<div class="col-12 no-results">
					{{ __('Tražena stavka nije pronađena.', THEME_TEXT_DOMAIN) }}
				</div>
				{!! get_search_form(false) !!}
			@endif

			@while($search_query->have_posts()) @php $search_query->the_post() @endphp
			<article class="col-12 col-md-6 post">
				<a href="{{ get_permalink() }}">
					@if(has_post_thumbnail())
						<div class="post__image">
							{!! \App\Traits\Image::getImage() !!}
						</div>
					@endif
					<h2 class="post__title">{!! get_the_title() !!}</h2>
				</a>
				<div class="post__content">
					<p class="post__date">{{ get_the_date() }}</p>
					{!! apply_filters('the_content',get_the_excerpt()) !!}
				</div>
			</article>
			@endwhile
		</div>
	</div>
@endsection
