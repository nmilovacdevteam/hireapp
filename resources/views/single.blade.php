@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    <section class="single_info">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 offset-lg-2">
                    <a onclick="history.go(-1);" class="back">
                        <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10" fill="none">
                            <path d="M4.75 1.5L1.25 5L4.75 8.5" stroke="#1089FF" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round"/>
                        </svg>
                        go back
                    </a>
                    <p class="posted">{{ __('Posted', THEME_TEXT_DOMAIN) }} {{ get_the_date('M d, Y',get_the_ID()) }}</p>
                    <h1>{!! \App\Controllers\App::title() !!}</h1>
                    @php $author_id = get_post_field( 'post_author', get_the_ID() );@endphp
                    <div class="author">{{ __('by',THEME_TEXT_DOMAIN) }} {{ get_the_author_meta( 'first_name', $author_id  ) }} {{ get_the_author_meta( 'last_name', $author_id  ) }}</div>
                </div>
            </div>
        </div>
        {!! \App\Traits\Image::getImage(['post_id'=>get_the_ID(),'image_sizes'=>['single_blog'],'image_classes'=>'featured_image']) !!}
        <div class="container">
            <div class="row">
                <div class="col-lg-1 offset-lg-1">
                    <div class="share_icons">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ get_the_permalink() }}" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21"
                                 fill="none">
                                <path d="M21 10.5C21 4.725 16.275 0 10.5 0C4.725 0 0 4.725 0 10.5C0 15.75 3.80625 20.0812 8.79375 20.8687V13.5187H6.16875V10.5H8.79375V8.1375C8.79375 5.5125 10.3688 4.06875 12.7313 4.06875C13.9125 4.06875 15.0938 4.33125 15.0938 4.33125V6.95625H13.7812C12.4688 6.95625 12.075 7.74375 12.075 8.53125V10.5H14.9625L14.4375 13.5187H11.9438V21C17.1938 20.2125 21 15.75 21 10.5Z"
                                      fill="#091D34"/>
                            </svg>
                        </a>
                        <a target="_blank" href="https://twitter.com/share?text={{ get_the_title() }}&url={{ get_the_permalink() }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="21" viewBox="0 0 26 21"
                                 fill="none">
                                <path d="M25.4182 2.45105C24.4834 2.86525 23.4793 3.14511 22.4236 3.2716C23.5129 2.61985 24.3278 1.59408 24.7163 0.38567C23.6929 0.993518 22.5729 1.42138 21.405 1.65065C20.6195 0.812028 19.5792 0.256179 18.4455 0.0693972C17.3118 -0.117385 16.1481 0.0753499 15.1352 0.617679C14.1223 1.16001 13.3167 2.02159 12.8436 3.06865C12.3705 4.11572 12.2563 5.28969 12.5188 6.4083C10.4452 6.30418 8.41671 5.76523 6.56491 4.82641C4.71312 3.88759 3.07942 2.56989 1.76985 0.958827C1.32207 1.73125 1.06459 2.6268 1.06459 3.58057C1.06409 4.43918 1.27553 5.28464 1.68015 6.04194C2.08477 6.79923 2.67006 7.44495 3.38409 7.92179C2.55601 7.89544 1.7462 7.67169 1.02206 7.26915V7.33632C1.02197 8.54055 1.43853 9.70773 2.20104 10.6398C2.96355 11.5719 4.02505 12.2114 5.20543 12.45C4.43725 12.6579 3.63186 12.6885 2.85011 12.5395C3.18315 13.5757 3.83186 14.4818 4.70545 15.1309C5.57903 15.7801 6.63375 16.1399 7.72195 16.1598C5.87467 17.6099 3.5933 18.3966 1.24483 18.3931C0.828817 18.3932 0.413161 18.3689 0 18.3203C2.38384 19.8531 5.15879 20.6665 7.99285 20.6633C17.5865 20.6633 22.8311 12.7175 22.8311 5.82618C22.8311 5.60229 22.8255 5.37616 22.8155 5.15228C23.8356 4.41453 24.7162 3.50097 25.4159 2.45441L25.4182 2.45105Z"
                                      fill="#091D34"/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="content_wrapper">
                        {!! apply_filters('the_content', get_the_content()) !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="category-sidebar">
                        <p class="text">{{ __('From this Category', THEME_TEXT_DOMAIN) }}</p>
                        <div class="posts_sidebar">
                            @php $args = [
	'posts_per_page'=>4,
	'post_type'=>'post',
	'tax_query'=>[
				'taxonomy' => 'category',
				'field'    => 'term_id',
				'terms'    => get_terms('category',get_the_ID())[0]
			]
]; @endphp

                            @php $query = new WP_Query( $args ); @endphp
                            @if ( $query->have_posts() )
                                @while ( $query->have_posts() )
                                    @php $query->the_post(); @endphp
                                    <div class="post">
                                        <div class="title">
                                            <a href="{{ get_the_permalink(get_the_ID()) }}">
                                                {{ get_the_title() }}
                                                {!! \App\Traits\Image::getImage(['post_id'=>get_the_ID(),'image_sizes'=>'small_single_blog']) !!}
                                            </a>
                                        </div>
                                        <div class="author">
                                            @php $author_id = get_post_field( 'post_author',get_the_ID() );@endphp
                                            <span class="name">{{ __('by',THEME_TEXT_DOMAIN) }} {{ get_the_author_meta( 'first_name', $author_id  ) }} {{ get_the_author_meta( 'last_name', $author_id  ) }}</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="2" height="2"
                                                 viewBox="0 0 2 2"
                                                 fill="none">
                                                <circle cx="1" cy="1" r="1" fill="#7D8EA0"/>
                                            </svg>
                                            <span class="date">
                                        {{ get_the_date('M d, Y',get_the_ID()) }}
                                    </span>
                                        </div>
                                    </div>

                                @endwhile
                            @endif


                            @php wp_reset_postdata(); @endphp


                        </div>
                    </div>
                    <div class="list_categories">
                        <div class="title">{{ __('More Categories', THEME_TEXT_DOMAIN) }}</div>
                        @foreach( get_the_category() as $category)
                            <a class="category"
                               href="{{ get_category_link( $category->term_id ) }}">{{ esc_attr($category->name) }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('partials.newsletter')
    @if($related_posts)
        <section class="related_articles">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>{{ __('Related Articles',THEME_TEXT_DOMAIN) }}</h3>
                    </div>
                </div>
                <div class="row">
                    @foreach($related_posts as $post)
                        <div class="col-lg-3">
                            <h3>
                                <a href="{{ get_the_permalink($post->ID) }}">{{ get_the_title($post->ID) }}
                                    {!! \App\Traits\Image::getImage(['post_id'=>$post->ID,'image_sizes'=>'small_single_blog']) !!}
                                </a>
                            </h3>
                            <div class="post_info">
                                @php $author_id = get_post_field( 'post_author', $post->ID );@endphp
                                <span>{{ __('by',THEME_TEXT_DOMAIN) }} {{ get_the_author_meta( 'first_name', $author_id  ) }} {{ get_the_author_meta( 'last_name', $author_id  ) }}</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="2" height="2" viewBox="0 0 2 2"
                                     fill="none">
                                    <circle cx="1" cy="1" r="1" fill="#7D8EA0"/>
                                </svg>
                                <span>
                            {{ get_the_date('M d, Y',$post->ID) }}
                        </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
    <section class="recent_articles">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3>{{ __('Recent Articles', THEME_TEXT_DOMAIN) }}</h3>
                </div>
            </div>
            <div class="row posts">
                @php $args = [
	'posts_per_page'=>3,
	'post_type'=>'post'
]; @endphp

                @php $query = new WP_Query( $args ); @endphp
                @if ( $query->have_posts() )
                    @while ( $query->have_posts() )
                        @php $query->the_post(); @endphp

                        @include('partials.posts')
                    @endwhile
                @endif


                @php wp_reset_postdata(); @endphp

            </div>
        </div>
    </section>
    @endwhile
@endsection
