{{--
Template Name: Terms pages
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    <section class="terms_picker">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-2 terms_selector">
                    <a data-select="privacy_policy" {{ is_privacy_policy() ? 'class=selected': '' }} >{{ __('Privacy Policy', THEME_TEXT_DOMAIN) }}</a>
                    <a data-select="toc" {{ is_privacy_policy() ? '': 'class=selected'}}>{{ __('Terms and Conditions',THEME_TEXT_DOMAIN) }}</a>
                </div>
            </div>
        </div>
    </section>
    <section class="terms_content">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-2 terms_switcher {{ is_privacy_policy() ? 'show' : '' }}"
                     id="privacy_policy">
                    <h1>{{ get_the_title(get_option( 'wp_page_for_privacy_policy' )) }}</h1>
                    {!! apply_filters('the_content', get_post_field('post_content', get_option( 'wp_page_for_privacy_policy' ))) !!}
                </div>
                <div class="col-lg-10 offset-lg-2 terms_switcher {{ is_privacy_policy() ? '': 'show'}}" id="toc">
                    <h1>{{ get_the_title() }}</h1>
                    {!! apply_filters('the_content', get_the_content()) !!}
                </div>
            </div>
        </div>
    </section>
    @endwhile
@endsection
